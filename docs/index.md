---
title: Home page for Vymo documentation
authors: AB
---

# Vymo documentation site

{% if audience == "internal" %}
**This page is meant for internal audience.**
{% else %}
**For Vymo customers**
{% endif %}

Vymo is an enterprise-grade sales assistant that enables sales and service teams to do their jobs while being mobile.

This document contains instructions to install, configure, and use Vymo.