# Glossary

!!! note ""
    This page is a work-in-progress.

{% from 'templates/glossary.json' import glossary %}
{% set gloss = glossary %}
{% for entries in gloss %}
   {% for key, value in entries.items() %}
      <p>Key: {{key}}, Value: {{value}}</p>
   {% endfor %}
{% endfor %}

<!--<dl>
<dt>allocation</dt>
<dd>&nbsp;...</dd>
<dt>user</dt>
<dd>A person logged in to the Vymo app.</dd>
<dt>lead</dt>
<dd>A person or a business entity that might eventually become a customer.</dd>
<dt>business rule</dt>
<dd>&nbsp;...</dd>
<dt>opportunity</dt>
<dd>Another word for <i>lead</i>.</dd>
<dt>contact</dt>
<dd>&nbsp;...</dd>
<dt>rule</dt>
<dd>&nbsp;...</dd>
<dt>rule set</dt>
<dd>&nbsp;...</dd>
<dt>parameter</dt>
<dd>&nbsp;...</dd>
<dt>expression</dt>
<dd>&nbsp;...</dd>
<dt>metric</dt><dd>&nbsp;...</dd>
<dt>geofencing</dt><dd>&nbsp;...</dd>
</dl>-->