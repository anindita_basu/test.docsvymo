---
title: Variables from the macros plug-in
author: AB
---

# Variables

!!! note "Test page: macros plug-in"
    This page is meant for testing things out.

The content on this page is generated through the `macros` plug-in from https://github.com/fralau/mkdocs_macros_plugin.

{{ macros_info() }}