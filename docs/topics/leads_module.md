---
title: Configuring the Leads module
author: AB
---

# Creating the leads module

!!! note ""
    This page is a work-in-progress.

## Specifying the leads parameters

...

## Setting up the leads states

...

## Setting up the leads workflow

...

## Setting up the leads dashboard

...



## Related pages

-  Input fields (x-ref, TBD)
-  Dashboard metrics (x-ref, TBD)
