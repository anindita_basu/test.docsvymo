---
title: Test page for internal doc site
author: AB
---

# Test page

!!! note "Sandbox"
    This page is meant for testing things out.

## Test area for variables

Full list of variables in the mkdocs_macro environment is at [macros_plugin.md](macros_plugin.md).

Doc site timestamp (macros plugin): {{ now() }} (time is in GMT)

Page update timestamp (MkDocs page attribute): {{ page.update_date }}

Page title (YAML frontmatter): {{ page.meta.title }}

## Test area for conditions

{% if audience == "internal" %}
!!! note "IF-ElSE test"
    This page is meant for internal audience.
{% else %}
!!! note "IF-ElSE test"
    This page is meant for external audience.
{% endif %}

## Test area for external variables

var1: {{ menu }}

var2: {{ button1 }}

var3: {{ sequence1 }}

## Test area for image maps

<img src="../../images/provisioning_workflow.png" alt="provisioning flowchart" usemap="#provisioning">
<map name="provisioning">
  <area shape="rect" coords="5,9,190,108" alt="Setting up leads parameters" href="../leads_module/">
  <area shape="rect" coords="330,9,511,108" alt="Setting up leads states" href="../macros_plugin/">
  <area shape="rect" coords="650,9,834,108" alt="Setting up leads workflow" href="../leads_module/">
  <area shape="rect" coords="974,9,1156,108" alt="Setting up leads dashboard" href="../leads_module/">
  <area shape="rect" coords="5,190,190,292" alt="Setting up the Users parameters" href="../topic_template/">
  <area shape="rect" coords="327,190,511,292" alt="Setting up the Users dashboard" href="../topic_template/">
  <area shape="rect" coords="647,190,834,292" alt="Allocating leads" href="../leads_allocate/">
  <area shape="rect" coords="967,190,1156,292" alt="Adding users" href="../topic_template/">
  <area shape="rect" coords="5,380,190,480" alt="Adding leads" href="../topic_template/">
</map>

## Test area for file includes

{% include 'topics/leads_rules_keys.md' %}




