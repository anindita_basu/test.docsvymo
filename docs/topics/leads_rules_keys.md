
<table>
<tr><th>Key</th><th>Description</th></tr>
<tr><td>users</td><td>Stores user information</td></tr>
<tr><td>loggedIn</td><td>Stores 'AVAILABLE' against every logged in user</td></tr>
<tr><td>userVsTenure</td><td>Calculated if the user inputs data contains a <i>date_of_joining</i> field.</td></tr>
<tr><td>allocatedLeadCount</td><td>Stores the number of leads automatically allocated to a user.</td></tr>
<tr><td>untouchedLeadCount</td><td>Stores the number of open leads for the past three months for a user.</td></tr>
<tr><td>pincodeVsUsers</td><td>Stores a list of users for each postal code if the user input data contains a value in the <i>pincode_mapped</i> field.</td></tr>
<tr><td>managerVsUsers</td><td>Stores a list of users for each manager.</td></tr>
<tr><td>managerVsAgents</td><td>Stores a list of territory users for each manager. Additionally, if the user input data contains a <i>allocateLeads</i> flag that is 'TRUE'.</td></tr>
<tr><td>branchVsUsers</td><td>Stores a list of users for each branch if the user input data contains a value in the <i>channelBranchCodes</i> field.</td></tr>
<tr><td>productCodeVsUsers</td><td>Stores a list of users for each product if the user input data contains a value in the <i> product_mapped field</i>.</td></tr>
<tr><td>leadContext</td><td>Stores lead data sent from LMS as a Kafka message.</td></tr>
</table>

