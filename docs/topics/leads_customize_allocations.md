---
title: Customizing lead allocation
---

# Customizing lead allocations

Automatic allocations look at leads one at a time. If you need rules that must evaluate several leads at one time, evaluate them against several sales people, and find a match, you need to code the rules into the client instance of the app. This is customized allocation.

Consider a scenario where you want to allot a lead to someone based on their relative distance, as compared to other sales people, from the lead location. Such a rule needs the following sequence:

1.  Fetch lead location
1.  Fetch locations of all sales people
1.  Find sales people <= lead location + 5
1.  Filter sales people list recursively to identify the sales person at minimum distance from lead location

Such allocations need customized rules. They cannot be set up through the self-serve dashboard.

## Example

Consider a scenario where you have a list of leads but before assigning them to anybody, you must ensure that:

1.  All available sales people are grouped into specific categories
1.  Within a category, all sales people are grouped into specific roles
1.  A lead is matched to a person only if a series of conditions are met 

This allocation expression first puts the sales people into groups, then finds leads within a location, and then uses a series of conditions to match a lead to a sales person. Many of the parameters used in the evaluation are custom attributes.

1.  Receive data from customer for sales person Attribute 1, Attribute 2, Attribute 3, and Attribute 4.
1.  If Attribute 1 = Condition 1, put sales person in Group 1 *ELSE* if Condition 2 is TRUE, put in Group 2 *ELSE* put in Group 3.
1.  Fetch lead location.
1.  Match lead location with Group, and fetch sales person in that group.
1.  If Attribute 2 of sales person in that group = Condition 2 *AND* Attribute 3 = Condition 3, allocate the lead to the sales person <br/>*ELSE*<br/>if Attribute 4 = Condition 4 *AND* Attribute 2 = Condition 5, allocate the lead to the sales person<br/>*ELSE*<br/>mark the lead for mannual allocation.

{% if audience == "internal" %}
A similar allocation logic was used for TVS. 

The following static attributes were used:

-  Sales region
-  Sales person efficiency
-  Sales person designation
-  Number of leads closed during a specific time period
-  Maximum number of leads that can be allocated to each designation 

The following conditions were used to determine optimized allocation:

-  Lead postal code
-  Sales person previously assigned to the lead
-  Number of leads open for a sales person
{% endif %}

## Related pages

-  Fields and data types (x-ref, TBD)

{% if audience == "internal" %}

## Review comment

[Leave a review comment](https://docs.google.com/spreadsheets/d/16CcIl7wpuhzyl3zVOxw_RJL_e-iovOTVALRETDH9Yr4/edit?usp=sharing)

{% endif %}
