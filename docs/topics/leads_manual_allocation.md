---
title: Manual lead allocation
---

# Allocating leads manually

You review a list of leads and assign them, one by one, to a person on the sales team. By default, all leads are sent into the manual allocation pipeline unless automatic allocation or customized allocation is set as the first choice. 

## Prerequisites

-  A list of sales people is available in the app. If not, see "Adding users (x-ref, TBD)".
-  The dashboard for manual allocation is enabled. If it isn't, see "Creating the Leads module (x-ref, TBD)".

## Steps

1.  Log in to the Vymo web app.
1.  Click **Leads** > **Allocations**.
1.  Select a lead. The map shows the location of all sales people who are in the same geographical area as the lead.
1.  Select a salesperson.
1.  Schedule a meeting, and click **Allocate**.

The lead is allocated, and the sales person gets an SMS notification. The sales person can choose to reschedule the call or visit, or reassign the lead to someone else.

## Limitations

You might need to remember the calendar and open lead counts of the sales people so that you can distribute the leads in a fair manner.

## Related pages

-  Enabling location tagging (x-ref, TBD)
-  Adding and uploading users (x-ref, TBD)
-  Adding and uploading leads (x-ref, TBD)

{% if audience == "internal" %}

## Review comment

[Leave a review comment](https://docs.google.com/spreadsheets/d/16CcIl7wpuhzyl3zVOxw_RJL_e-iovOTVALRETDH9Yr4/edit?usp=sharing)

{% endif %}