---
title: Rule attributes and conditions for automatic allocations
---

# Parameters for automatic allocations

You identify certain parameters and use them to define rules that will automatically match leads to sales people. Examples of such parameters are a sales person's location, availability, efficiency, or base branch, a lead's location, a sales person's reporting hierarchy, or a lead's history.

These parameters are taken from the attribute fields that were used when uploading or adding leads and sales people to the Vymo app. For more information on these attributes, see "Fields and data types (x-ref, TBD)".

Additionally, certain parameters might need calculations or are pulled as real-time data. For example, a person's location at the time of login is real-time data, or a person's efficiency calculated as the ratio of number of leads allotted to them to the number of leads closed within a specified time is a derived value. 

The following parameters are built into the Vymo app{% if audience == "internal" %}; additional parameters need customization work from the Solutions Delivery team{% endif %}.

-  Metrics: Open leads count
-  Metrics: Allocated leads count
-  Metrics: All logged in users
-  Metrics: All users reporting to the person who created the lead
-  Metrics: Allocation time within specific hours (Custom)
-  Metrics: Distance between lead and all hierarchical users within a radius (Custom)

Based on these parameters, here are some rules that can be used for automatic allocations:

-  Lead attribute *equal to* OR *greater than* OR *lesser than* OR *contains* OR *same as* OR *not contains* OR *not same as* Some value
-  User attribute *equal to* OR *greater than* OR *lesser than* OR *contains* OR *same as* OR *not contains* OR *not same as* Some value
-  Lead attribute *same as* OR *not same as* OR *contains* User attribute
-  User attribute *same as* OR *not same as* OR *contains* Lead attribute
-  All users with status *equal to* loggedIn

The logical operators that you can use to build the evaluation expression depend on the data type of the attribute you use. For example, text data do not lend themselves to comparisons such as *greater than* or *lesser than* but can be used for operators such as *equal to* or *not equal to*.

{% if audience == "internal" %}
Parameters are stored as key-value pairs in JSON. Here is an example:
```json
{
    "key": {
        "value": "attr1",
        "type": "lead-attribute"
    },
    "operator": "same-as",
    "operand": [{
        "type": "STATIC",
        "value": "some value"
    }]
}
```

Here is a list of the available inbuilt attributes:

{% include 'topics/leads_rules_keys.md' %}
 
{% endif %}

## Related pages

-  [Examples of allocation rules](leads_rules_examples.md)
-  [Customizing lead allocations](leads_customize_allocations.md)

{% if audience == "internal" %}

## Review comment

[Leave a review comment](https://docs.google.com/spreadsheets/d/16CcIl7wpuhzyl3zVOxw_RJL_e-iovOTVALRETDH9Yr4/edit?usp=sharing)

{% endif %}
