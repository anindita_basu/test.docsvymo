---
title: Creating leads
author: AB
---

# {{ page.meta.title }}

!!! Important "Work-in-progress"
    This page is under construction.

## Upload several leads at once

...

## Add a lead

...

## Related pages

-  Input fields (x-ref, TBD)
-  [Creating the leads module](leads_module.md)

## Old text dump

Sales Agents can create Leads via Vymo Web or Vymo Mobile App.

Web: Log in to ...
Mobile: Log in to ...

Updates to lead states regarding follow-up dates are added to the Vymo Calendar automatically
Interactions with the client will be captured by the Sales Agent as part of the lead workflow
Managers can view the real-time updates on leads

Save Draft

You can save your work without entering all the information while creating a Lead. This enables users to pause work with limited information and resume when data is available.  

Learn more about draft functionality.

View Lead

You can view the lead information by navigating to the lead list and selecting a lead. You can view the lead profile information, business generated, completed and upcoming activities with the lead.

Update Lead

Export Leads

You can download Lead details in bulk from the Lead list view from the Vymo web application. You can perform necessary computations and prepare relevant reports. The default filter is Month To Date (MTD). You can extend the date range upto 60 days. 

Login to your Vymo web application > Leads > Leads List > Click on Download CSV.

You can refer the download request by navigating to Vymo Web application > Others menu > Downloads and download the data.
 
The export file will be available upto 1 hour after the download request completes.
If you are not able to view the Others menu in your Vymo web application, contact Vymo Support and we will help you out. 

Lead Import

If you have to import a number of leads at once, you can use the Lead Bulk Import functionality. 
 
How to Import Leads

In order to import, login to the Vymo Web application > navigate to Leads > Leads List > Bulk Import > select a file > Upload. Multiple users can upload leads at the same time.

Prerequisites:

1.	Files in .xls/.xlsx/.csv formats will only be allowed to be imported.
2.	Maximum file size allowed is 100MB.
3.	As per an internal benchmark, 100000 leads can be imported at once
4.	If you import the same file or leads list, duplicates will be ignored.
5.	Localization (support for different locale) is not supported as part of bulk import.

After Import

1.	Once the import process is complete, the original file (uploaded) and the summary file will be available for download.
2.	The summary file will be sent via email as well.
3.	The data will be imported as Leads in your Sales Leads list. 



