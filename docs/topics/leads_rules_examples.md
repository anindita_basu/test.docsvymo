---
title: Lead allocation: Use cases
---

# Examples of allocation rules

Allocation rules are configured as expressions that return a filtered user set. An expression can contain several rule sets, which are evaluated in the order in which they are defined, with an OR logic. Each rule set can contain several rules, which are evaluated in the order in which they are defined, with an AND logic.

## Example 1 (Configurable through self-serve with inbuilt metrics)

Consider a scenario where you allocate leads to a person according to the product that the lead is interested in and the sales person who is attached to a branch nearest to the lead and holds a specific designation.

This allocation expression will have several rule sets, each with several rules. Each of these rules use an inbuilt metric (such as branch, product, and role).

<ol>
<li>Rule set 1:
   <ol><li>Lead product = ProductX <i>AND</i></li>
   <li>User branch = BranchP <i>AND</i></li>
   <li>User role = RoleA</li></ol><i>ELSE</i></li>
<li>Rule set 2:
    <ol><li>Lead product = ProductX <i>AND</i></li>
	<li>User branch = BranchP <i>AND</i></li>
	<li>User role = <u>RoleB</u></li></ol><i>ELSE</i></li>
<li>Rule set  3:
    <ol><li>Lead product = ProductX <i>AND</i></li>
	<li>User branch = BranchP <i>AND</i></li>
	<li>User role = <u>RoleC</u></li></ol><i>ELSE</i></li>
<li>Rule set  4:
    <ol><li>Rule 1: Lead product = ProductX <i>AND</i></li>
	<li>User branch = BranchP <i>AND</i></li>
	<li>User role = <u>RoleD</u></li></ol><i>ELSE</i></li>
<li>Rule set  5:
    <ol><li>Lead product = <u>ProductY</u> <i>AND</i></li>
	<li>User branch = BranchP <i>AND</i></li>
	<li>User role = <u>RoleA</u></li></ol><i>ELSE</i></li>
<li>Rule set  6:
    <ol><li>Lead product = ProductY <i>AND</i></li>
	<li>User branch = BranchP <i>AND</i></li>
	<li>User role = <u>RoleB</u></li></ol><i>ELSE</i></li>
<li>Rule set  7:
    <ol><li>Lead product = ProductY <i>AND</i></li>
	<li>User branch = BranchP <i>AND</i></li>
	<li>User role = <u>RoleC</u></li></ol><i>ELSE</i></li>
<li>Rule set  8:
    <ol><li>Lead product = ProductY <i>AND</i></li>
	<li>User branch = BranchP <i>AND</i></li>
	<li>User role = <u>RoleD</u></li></ol><i>ELSE</i></li>
<li>Rule set  9:
    <ol><li>Lead branch = BranchP <i>AND</i></li>
	<li>User role = <u>Boss</u></li></ol></li>
</ol>

These rules are evaluated sequentially, and every time a condition in a rule evaluates to FALSE, the next rule is taken up for evaluation. The last rule is the fallback rule, where all leads that still remain unallocated at the end of Rule 8 and allocated automatically to the branch head through Rule 9.

{% if audience == "internal" %}
A similar allocation logic was used for ApolloMunich.
{% endif %}

## Example 2 (Configurable through self-serve with custom metrics)

Consider a scenario where you want to automatically allocate a lead to the first available sales person who is within a 5 km radius of a lead and has fewer than 4 open leads only if the time is between 7:00 a.m. and 7:00 p.m.

This automatic allocation needs 2 rule sets, where the first rule set has 4 rules and the second, 5 rules. In addition to inbuilt metrics (such as open leads, lead location), these rules also use real-time data (time of the day) and derived data (location within a radius), which are custom metrics that must be coded by the Solutions Delivery team before they can be used on the self-serve page.

1.  Rule set 1:
    -  Time is between 7:00 a.m. and 7 p.m.
	-  Product list of lead is contained in product list of user
    -  Open leads assigned to user < = 3
	-  User location <= 5 kms + lead location
1.  Rule set 2:
	-  Time is not between 7:00 a.m. and 7 p.m.
	-  Product list of lead is contained in product list of user
	-  Postal code of lead = postal code of user
	-  User reports to the person who created the lead
    -  Open leads assigned to user < = 20

This rule set is evaluated the following manner:

![rules flowchart](../../images/ruleSets_1.png)

{% if audience == "internal" %}
A similar allocation logic was used for ICICI.
{% endif %}

## Example 3 (Not configurable through self-serve)

Consider a scenario where you must evaluate several leads and several sales people at the same time, and find an optimum match. For example, you look at all leads in a specific area and then at all sales people in that area, and match a lead to the person closest to that lead. Because such an allocation does not work if you look at leads one by one, as in automatic allocation, the rules cannot be set up through the self-serve page. For an example, see [Customizing lead allocations](leads_customize_allocations.md).

## Related pages

-  [Parameters for auto-allocation rules](leads_rules_parameters.md)

{% if audience == "internal" %}

## Review comment

[Leave a review comment](https://docs.google.com/spreadsheets/d/16CcIl7wpuhzyl3zVOxw_RJL_e-iovOTVALRETDH9Yr4/edit?usp=sharing)

{% endif %}