# Send Group Notifications

You can send important announcements and messages to your sales team in bulk.

Sales Agents will receive the message as a notification on their Vymo mobile app. The message will also be available for the sales person any time in their Vymo app notification section. 
 
## Bulk Notification

The bulk notification feature is available to the managers and admins users through your Vymo web app.

You can send In-app messages to your team members registered in Vymo. These messages will be received as app notification in your team members’s Vymo app.

You need to submit the message and a title along the ID of your team members in an xls template file that is provided in the bulk notification upload.

The message will be delivered to user’s app as Vymo notification and will be visible in the notification screen.

## How to use Bulk notification? (Available on Vymo Web App)

Login to Vymo web application as an administrator or using the user account with ‘Manager User’ access. The bulk notification feature can be accessed with the ‘Bulk Notification’ Button provided on top of the user list.

The ‘Bulk Notification button will open the notification uploader along with the link to download a sample template.

You can download the sample template file (.xls) which would have the format for uploading the notification data to the system. 
 
Note:

1.	Make sure the column headers are same as that in the template file.
2.	The name of the sheet is kept as ‘Sheet1’
 
Format of the bulk notification template:

1.	User Id: This is the user ID of user accounts created in Vymo. You can find the user ID for each user from ‘Manager user’ dashboard or taking a bulk export of all user info using ‘Download List’.
2.	Title: The tile that you want to display for the notification.
3.	Description: The main content of the notification. If the message is long, user will be able to expand the notification from the notification menu or read it inside Vymo’s notification section.

Once the file is uploaded and processed, the message will be sent to your team members specified in the file. 
 
In case of an error, you will be provided with an error report which can be used to fix the issue.
 
## Limitation

1.	There is a character limit on the title and the message content. Title - 50, Message - 500 
2.	The maximum records allowed per upload is 400 rows. If you wish to send a notification to more than 399 user, you will have to do that in batches.
3.	The file should have the same ‘Sheet name’ and the column headers as in the sample template.

## FAQ

**Can I receive notification in my Vymo web application?**

No, the notification is received only on the user’s mobile app.

**Can I send bulk notification using Vymo mobile app?**

Not at the moment, currently you can use the bulk notification feature only through the web application.

**Who can send a Bulk notification?**

Admin users can use the bulk notification feature. If you don’t see the bulk notification option please confirm if you are an admin user.

**What is user ID?**

The user ID is same as the ID of your Vymo users. If you have given the Vymo ID as the employee ID, fill the User ID column in the file with the employee ID of the user who needs to receive the message.

**Can I send a notification to a team member who is not registered in Vymo?**

No, the user must be registered in Vymo in order to receive the notification. In case the ID in the file does not match any user in your Vymo app, you will receive an error message. You will have to download the error report and try again.

**What will happen if the phone is not connected to the internet while sending a bulk notification?**

The message will be delivered when the user’s app comes online. The message will also be available in the notification screen in the user’s Vymo app.

**Where can I see the errors reports after submitting the bulk notification?**

In case there was an error while processing the bulk notification file, you will be provided with an error report. You can download this file from the same upload screen. The errors will be mentioned as an additional column in your file.
