# Billed Users List

This report enables Vymo Admin to view the list of users who are billed for on a given period of time.

Login to your Vymo Web Application > Reports > All Reports > User Reports > Billing Report - User List.
  
## Billing Report Metrics

The details in Billing Report are:

<table>
<tr>
<th>Report Column</th>
<th>Description</th>
</tr>
<tr>
<td>User Details</td>
<td>User details includes the User ID, Name, Email and Phone Number</td>
</tr>
<tr>
<td>Client ID</td>
<td>Identification of the client the user belongs to</td>
</tr>
<tr>
<td>Date</td>
<td>Date when maximum users were enabled</td>
</tr>
</table>

!!! note "Note Points"
    -  Vymo provides an option to allow/restrict over-usage. If it is allowed, then the Vymo admin in your organization can enable/create users more than the licensed user limit.
    -  If your organization has users using Vymo from multiple business divisions/channels, you can also set channel level user limits. 
    -  If the channel level limit is set, the admin will not be allowed to enable/create more users above the set limit, irrespective of the fact if over-usage is allowed at the client level or not.
