# Sync Overview

Here’s a quick overview of how data is mapped between Vymo and Salesforce:

Here’s more details about how each module is synced between Vymo and Salesforce:

## Business Objects

-  MASTER: Salesforce
-  Leads, Opportunities, Accounts, Contacts are considered as Business Objects in Salesforce
-  Business objects in Salesforce with a workflow assigned (Leads, Opportunities) will be created as Lead Modules in Vymo
    -  History of state transitions will also be imported into Vymo
-  Business objects in Salesforce without workflow (ACCOUNTS, CONTACTS) will be created as Partner Modules in Vymo
-  Bulk operations on these entities in Vymo will be disabled
    -  Bulk upload of Leads/ Opportunities/ Accounts/ Partners

## Custom Objects 

-  MASTER: Salesforce
-  Custom objects can be mapped as Lead or Partner Modules in Vymo. Contact Vymo Support to help with the mapping
-  Bulk operations on Custom Objects/Modules in Vymo will be disabled
-  Custom object modules created in Vymo will not sync to Salesforce

## Field Mapping
-  MASTER: Salesforce
-  Fields in Salesforce will be created as custom input fields in Vymo, during the initial setup automatically
-  Note: 
    -  Field validations (such as min-max, or characters not allowed etc.,) or custom rules per field, will not be imported to Vymo, validations should be configured in Vymo manually.
    -  Field grouping will not be imported from Salesforce, grouping should be configured in Vymo manually.
    -  After the setup is complete, new fields added, updates to existing fields, updates in field validation or grouping should be updated in Vymo manually.

## Activity Mapping

-  Events/ Tasks in Salesforce will be created as Activities in Vymo
-  Activities created in Vymo will be created as Events in Salesforce
-  New activities and updates to existing activities will be exported to Salesforce immediately

## User Sync

-  MASTER: Salesforce
-  Users present in Salesforce will be created in Vymo during initial setup
-  Users created in Salesforce after the integration is setup, will be imported into Vymo. However, hierarchy changes, such as Manager re-assignment will not be updated in Vymo, this should be done manually, contact Vymo Support to help with this.
-  User hierarchy definition import to Vymo 
    -  Users and Managers will be created as per Vymo’s user hierarchy capabilities
-  You cannot Create/Upload users in bulk in Vymo
-  User profile updates cannot be made in Vymo. It is recommended to update user profile updates in Salesforce directly. Contact Vymo Support to help with the updates in Vymo.
-  Users deleted (archived) in Salesforce will be deleted (archived) in Vymo
-  If there is a change in user-manager hierarchy in Salesforce, activities should be reassigned in Vymo. Contact Vymo Support to learn how. 
-  When a user is disabled in Salesforce, Activities will not be re-assigned in Vymo. Contact Vymo Support to help.

<table>
<tr>
<th>Vymo Field</th>
<th>Vymo Field Values</th>
<th>Salesforce Field</th>
<th>Salesforce Field Values</th>
<th>Description</th>
</tr>
<tr>
<td>code</td>
<td>&nbsp;</td>
<td>id</td>
<td>&nbsp;</td>
<td>id will be used as the duplicate check criterion</td>
</tr>
<tr>
<td>name</td>
<td>&nbsp;</td>
<td>name</td>
<td>&nbsp;</td>
<td>First name + Middle Name + Last name</td>
</tr>
<tr>
<td>mapping_login_id</td>
<td>&nbsp;</td>
<td>Username</td>
<td>&nbsp;</td>
<td>Used for Salesforce OAuth flow</td>
</tr>
<tr>
<td>email</td>
<td>&nbsp;</td>
<td>email</td>
<td>&nbsp;</td>
<td>Another user with same email will not be created in Vymo</td>
</tr>
<tr>
<td>parent</td>
<td>&nbsp;</td>
<td>Manager_ID</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>type (disable</td>
<td>&nbsp;</td>
<td>Type</td>
<td>Region;Territory/Branch</td>
<td>If User is Manager, type = Branch; If user is Sales user, type = Territory</td>
</tr>
<tr>
<td>Disabled</td>
<td>&nbsp;</td>
<td>isActive</td>
<td>Yes, No</td>
<td>Only active users will be synced to Vymo</td>
</tr>
<tr>
<td>auth_type</td>
<td><ul><li>Vymo</li><li>OAuth to Salesforce</li></ul></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>Will be set as OAuth to Salesforce for the SFDC integration</td>
</tr>
</table>

## User Management

-  All attempts to import data from Salesforce to Vymo will be via an integration user role with API-only access
-  All attempts to export data from Vymo to Salesforce will be using the logged-in user’s credentials to maintain user specific activity log in Salesforce
