# Overview

Modules define your Business processes in Vymo. Modules can be configured and customised for each customer, industry use case. There are 3 key modules in Vymo.

<table>
<tr>
<th>Module</th>
<th>Purpose</th>
</tr>
<tr>
<td>Lead</td>
<td>Define lead to opportunity, opportunity to customer and other workflows</td>
</tr>
<tr>
<td>Partner</td>
<td>Used for managing Partner relationship</td>
</tr>
<tr>
<td>Activity</td>
<td>Define activities to be completed by the users</td>
</tr>
</table>

You can model your business process in Vymo which will be executed for every business activity, such as creating a lead or partner in Vymo. 

-  It represents the business processes your organization follows Complex, custom, linear & non-linear workflows 
-  Collect inputs required in each state using forms
-  Leads through the defined workflow
-  Customize lead workflow and input forms 
-  Define validation rules for fields & forms
-  Each step in the workflows is a state
 

