# Business Card Scanning

Business card scanning can quickly transfer leads/contact information into digitally transcribed contacts. With Business Card scanning, you can add Leads, Partners or Contacts. 

!!! note ""
    -  This feature may be on a different pricing tier. Contact Vymo Support to enable this feature on your account. 
    -  If you need more than 10 scans per user, per month, contact Vymo Support. 
    -  Business cards in English will only be uploaded.

## Steps to follow

Here's how you can scan a business card and upload the contact information:

1.	Login to Vymo Mobile App > navigate to Add Lead > Scan Card
2.	Capture the information from the Business Card
3.	Review the information

When cards are scanned, the information is matched with the attributes in your application, such as Name, Phone Number, Address etc.

-  Place the Business card on a table/surface and have good internet connectivity Accuracy of the card scan depends on the Business card type, quality, card orientation, phone camera and network connectivity
-  If there is a mismatch, you can choose in which attribute the information should be populated.
