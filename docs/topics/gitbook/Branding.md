# Branding

Configure the branding as per your design guidelines.

Login to Vymo Web application > navigate to Global Settings > Branding and upload the details:

-  Company Logo
-  Primary Color
