# Best Route

Planning for the best route for the day is now at your fingertips, powered by Vymo's Route feature.

Users can view their Sales activities and meetings with Leads/Partners in the Calendar view. Additionally, they can view the best route to be taken to meet the leads/ partners to be met, based on the activities scheduled for the day. The route will be sorted by Meeting time.

You can view the Route by logging in to Vymo Mobile app > Home screen shows the Calendar > click on View Map and you can view the Route for the day.

On the route map, you can do the following:

-  View Lead/Partner information 
-  Initiate a call with the Lead/Partner 
-  Update Meeting Status 