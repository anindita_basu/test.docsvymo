# Overview

The Partner Relationship Management module helps manage engagement with Partners, Merchants and Agents.

You can capture activities with existing customers and/or business partners which eventually leads to acquiring new leads and opportunities.

-  It does not have a workflow  
-  Can be Active or Inactive 
-  Attributes can be defined for Relationship module  

Engage with the partners with quick actions:

-  Call 
-  Navigate
-  Email
-  Text/SMS 

## Create Partner

Information captured while creating a Partner profile:

-  Name
-  Phone
-  Email
-  Office Address
-  Home Address
-  Status
-  Tier
-  Recruitment date
-  Engagement frequency

And other fields based on the use case

## Partner Business Metrics

Important metrics measured as part of the Partner module are:

-  New leads generated
-  Revenue
-  Satisfaction

The definition of each metric can be different as the relationship with every partner will be unique. The data of business metrics is usually stored in business systems, data warehouses and these can be imported into Vymo via API integration.

!!! note "Note"
    Due to high data volume, initial data load in Vymo will be a full refresh. The subsequent data refreshes will be incremental. Incremental data refresh is throttled to 100MB per Relationship module.
 
-  Up to 7 metrics per Partner module 
-  Up to 3 years of monthly data & MTD for current month 
-  Data refreshed from client systems via Integration

## Activities with Partners

The Activities module can be used to configure declarative and detected activities. Each activity defined in the Activities module can be bound to one RM module, enabling users to create the given activity against the RM entity, like a partner or an existing customer. 

For instance, if a Business Planning activity is configured for an Agency RM module, the user can create that activity against any Agency partner.

These activities are called VO activities since they are bound to Vymo Objects [VO].

There are 2 ways in which the user can create Activities on an RM entity

-  Via the Schedule / Log activity option in the Hello screen
-  Via the Schedule / Log activity option in the RM entity summary page

<table>
<tr>
<th>Number of States in workflow</th>
<th>0</th>
</tr>
<tr>
<td>Number of Fields in a single RM module</td>
<td>255</td>
</tr>
<tr>
<td>Number of business metrics</td>
<td>7</td>
</tr>
<tr>
<td>External refresh of business metrics</td>
<td>100MB per refresh</td>
</tr>
<tr>
<td>Business metric refresh frequency</td>
<td>Weekly</td>
</tr>
<tr>
<td>Business metric refresh</td>
<td>Incremental only</td>
</tr>
</table>

## Partner Recruitment

-  Partner recruitment process is modelled as a Business process workflow (Lead nurturing workflow) 
-  Leads marked as “Won” in this flow will create a Partner in Vymo 
-  These Partners could have been converted in Vymo or in an external system

## Partner Tiers

Partners can be segmented in different tiers based on their performance.

-  Partner tiers determine the level of engagement & expected outcomes from the Partner
-  You can assign upto 5 Tiers in Vymo - Platinum, Gold, Silver, Bronze & None 
-  Tiers can be updated periodically based on the ongoing performance

Any number of tiers can be defined as long as these are mapped to one of the 4 predefined tier types (i.e. Platinum, Gold, Silver, Bronze). For each tier there is a name (Label) which can be customized and each Tier also has an associated icon, which can be recolored as well, in this case the star icon will be shown with the color configured for the tier.

## Partner Allocation

Partner can be allocated manually or automatic. You can define the allocation rules in Vymo to automate the allocation process.

## Partner Prioritization

You can define rules to prioritize which Partners should be met first. 

-  Priorities will be added as tags (Yes, No) or (Hot, Warm, Cold)
-  Priorities are updated based on Sales Agent activity or via Vymo
-  Partner can have a Tier and a Priority, such as, Platinum and Warm

## Partner Referral

Referral links all records across Vymo for the Partners:

-  Customer
    -  Contacts
    -  Opportunities 
-  Contact 
    -  Customer
    -  Opportunities 
-  Bank Branch 
    -  Leads
    -  Renewals Leads
    -  Contacts

## Referrals

Users can quickly find related records by using the Search functionality to find & populate records in context. The list of records is sorted by Name by default, however if there are over 500 search results the list is not sorted.
