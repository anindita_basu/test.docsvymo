# Vymo Standard Support - SOP

This document (or SOP) is applicable for supporting queries in the English language (email and phone). For other language support, please talk to the Vymo Sales team for Premium Support.

1.	Vymo Standard Support (referred to as VSS from now onwards) covers all the queries/incidents related to Vymo product offering only. e.g. Login related, Hello Card related issues.
2.	All the queries/incidents to VSS are expected to be coming from the Client L1 SPOC, post the initial validation of the same. Please note that VSS does not support queries from end users directly.
3.	All incidents should be sent to VSS either by sending an email to support@getvymo.com or by calling the helpdesk phone number.
4.	All the queries/incidents are created in Freshdesk as tickets.
5.	Support team will resolve queries/incidents as per Severity. VSS and Client SPOC will work together to reprioritize tickets to a lower/higher severity levels, during the ticket lifecycle.
6. Monthly Support tracker to be sent across to Client SPOC. All the incidents will be closed with communication as per published timelines.
7. In case the incident is classified as a Change Request aka CR (and not a Bug), the process for new CR will be initiated after mutual agreement between the Client SPOC and VSS team. In this case, VSS will handover the case to Vymo CSM team. Vymo CSM team will get int touch with the client SPOC to take this forward.

Example of Bug: A field documented in the config sheet is not available in the mobile app.

Example of CR: A field that is not documented in the config sheet is not available in the mobile app.

## High Severity Incidents (S1 & S2)

1.	VSS is available 24x7 (And 365 days) for Severity 1 and Severity 2 incidents.
2.	In case the incident happens outside the VSS Timings, please call one of the numbers provided in the escalation matrix below.
3.	Any security incident reported is automatically classified as Severity 1 and will be acted upon with highest priority. If required, VSS recommends the services to be shutdown for end users till the incident is resolved or patched.

### If the incident has originated at Client end and is being resolved by the Client

1.	Client SPOC to keep the above team informed on email and phone call on the above numbers in case there is an Incident Reported at the Client end that can impact the Vymo users.
2.	Client SPOC to share the timelines for resolution along with communication on email and confirmation once the resolution has been achieved.
3.	RCA to be shared to the Vymo Support Team and the Project Team.

### If the incident is originated at Vymo end and is being resolved by Vymo

1.	VSS will keep the Client SPOC informed of the Severity of the Incident and communicate as per agreed timelines for Resolution and Response.
2.	RCA to be shared with the Client SPOC as per timelines.

## Incident Details (For Client SPOC)

Please provide the below mandatory details as a part of the incident reporting. Without these details, it will take more iterations (and hence more time) to respond and resolve the incident. Please note, this only applies when the issue is being identified and reported by the Client SPOC.

1.	Number of user(s) impacted
2.	VYMO Login ID(s) (sample IDs of impacted users)
3.	Incident reported in App (Android or iOS or both) or Web or both.
4.	Detailed Incident Summary & Description
5.	Screenshot(s) & Videos of the incident reported

## Expectations from Customer end

1.	Customer will setup an in house Support team (We will refer to this as Client L1 team from now onwards) to handle basic queries from Vymo end-users. This team is expected to be headed by a Support lead who will be the SPOC for VSS team.
2.	Client L1 team will respond to all standard queries related to Vymo application. This team will address & interface with end users to resolve incidents including, but not limited to, password reset, creating/disabling user accounts, liaising with internal IT teams(and any other vendors or partners) for data, logs etc that are necessary to debug incidents.
3.	VSS team will help train and onboard the Client L1 team.
4.	In case Client L1 team need further support in resolving the incident, they can contact VSS by sending an email to support@getvymo.com
5.	Only incidents raised to the above Support email ID will be available for reviews, reporting purpose and fall under Resolution process.

## VSS Timings

### India (UTC+5:30)

<table>
<tr>
<th>Working Days</th>
<th>Timings</th>
</tr>
<tr>
<td>Mon-Fri</td>
<td>9:00AM-6:00PM</td>
</tr>
<tr>
<td>Saturday (1st, 3rd and 5th)</td>
<td>9:00AM-6:00PM</td>
</tr>
<tr>
<td>Saturday (2nd, 4th)</td>
<td>Unavailable</td>
</tr>
<tr>
<td>Sunday</td>
<td>Unavailable</td>
</tr>
<tr>
<td>All Public Holidays</td>
<td>Unavailable</td>
</tr>
</table>

###  UAE/Dubai (UTC+4:00)

<table>
<tr>
<th>Working Days</th>
<th>Timings</th>
</tr>
<tr>
<td>Mon-Thu</td>
<td>9:00AM-6:00PM</td>
</tr>
<tr>
<td>Saturday</td>
<td>9:00AM-6:00PM</td>
</tr>
<tr>
<td>Sunday</td>
<td>9:00AM-6:00PM</td>
</tr>
<tr>
<td>Friday</td>
<td>Unavailable</td>
</tr>
<tr>
<td>All Public Holidays</td>
<td>Unavailable</td>
</tr>
</table>

### Vietnam & Thailand & Indonesia (UTC+7:00)

<table>
<tr>
<th>Working Days</th>
<th>Timings</th>
</tr>
<tr>
<td>Mon-Fri</td>
<td>9:00AM-6:00PM</td>
</tr>
<tr>
<td>Saturday (1st, 3rd and 5th)</td>
<td>9:00AM-6:00PM</td>
</tr>
<tr>
<td>Saturday (2nd, 4th)</td>
<td>Unavailable</td>
</tr>
<tr>
<td>Sunday</td>
<td>Unavailable</td>
</tr>
<tr>
<td>All Public Holidays</td>
<td>Unavailable</td>
</tr>
</table>

## Vymo Severity Definitions

### Severity 1 (Urgent)

-  **Definition:** More than 25% of users impacted.
-  **Examples:** Login is not working
-  **Action:** <ul><li>Automated Response within 2 mins, once VSS gets email.</li><li>From Vymo: Communication from VSS or Customer Support Manager via email and phone call within 5 mins of a Sev1 issue being identified at Vymo.</li><li>From Client: Email & Phone Call from Client SPOC to VSS and Vymo Customer Support Manager.</li><li>Notify the client within 15 mins with issue validation, from the time Vymo Support gets phone call from the client or from the time Vymo's internal monitors detects the same.</li><li>All hands on the deck. Send regular emails to client about the status every 30 min.</li></ul>
-  **Resolution:** 
-  **RCA:** RCA should be available and provided to client within 5 business days.

### Severity 2 (High Severity)

-  **Definition:** More than 10% up to 25% of all users impacted for one client.
-  **Examples:** Existing users are able to continue working, however, new logins are impacted.
-  **Action:** <ul><li>Automated Response within 2 mins, once VSS gets email.</li><li>From Vymo: Communication from VSS or Customer Support Manager via email and phone call within 60 mins of a Sev2 issue being identified at Vymo.</li><li>From Client: Email from Client SPOC to Vymo Customer Support Manager or VSS Email ID support@getvymo.com as soon as a Sev2 issue is identified by the Client.</li><li>Notify the client within 1 hour with issue validation, from the time VSS gets phone call/email from the client or from the time Vymo's internal monitors detects the same.</li><li>Send regular emails to client about the status every 4 hours.</li></ul>
-  **Resolution:** <ul><li>Permanent Resolution or acceptable work around, within 16 hours from the time, the incident is reported.</li><li>In case of Android App issue, we will provide a fix and make it available on Vymo hosting, so that users can be unblocked immediately.</li><li>For iOS, we use code push mechanism (Requires a stop and start of app to get new code changes), wherever applicable (Generally 50% of issues falls under this). Only in cases we need to go through AppStore submission (For binary builds), the SLA cannot be met and depends on approvals from Apple.</li></ul>
-  **RCA:** RCA should be available within 5 business days for internal teams.

### Severity 3 (Medium Severity)

-  **Definition:** Less than 10% of users impacted for one client.
-  **Examples:** Edit profile fields not editable
-  **Action:** <ul><li>Automated Response within 2 mins, once VSS gets email.</li><li>Notify the client within 8 hours with issue validation, from the time Vymo Support gets phone call/email from the client or from the time Vymo's internal monitors detects the same.</li><li>Client will be provided with the release date based on the effort. Client facing teams to communicate any changes.</li></ul>
-  **Resolution:** Permanent Resolution or acceptable work around, within 5 working days from the time the incident is reported.
-  **RCA:** Sev 3 incidents with a recurrence of more than 5 times a week, need to be provided a RCA within 10 days.

### Severity 4 (Low Severity)

-  **Definition:** Less than 1% of users impacted for one client.
-  **Examples:** Adoption report is not showing data for older than 30 days
-  **Action:** <ul><li>Automated Response within 2 mins, once VSS gets email.</li><li>Notify the client within 3 working days with issue validation, from the time Vymo Support gets phone call/email from the client or from the time Vymo's internal monitors detects the same. (Sat, Sun and public holidays in India are not considered working days).</li><li>Client will be provided with the release date based on the effort. Client facing teams to communicate any changes.</li></ul>
-  **Resolution:** NA
-  **RCA:** NA
