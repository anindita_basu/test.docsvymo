# Lead Allocation

Allocate leads or prospects to the Sales Agents by defining rules or conditions. Based on the rules, all leads created in Vymo will automatically get assigned to Sales agents.

To assign leads using automatic allocation, 

1.	Login to Vymo Web application > navigate to Leads section > Allocation Settings
2.	Create the rules as per your requirement
3.	The sequence in which you define the rules is the sequence in which the rules will be executed.
4.	Use conditions while defining the rules (and, or) and (equal to, not equal to) to select and match the criteria.
