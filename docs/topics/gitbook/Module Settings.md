# Module Settings

Modules in Vymo are your Business entities you will operate with.

-  For instance, your sales teams can work with Leads via Direct channel
-  Or, your sales teams can acquire Partners/Agents as Resellers

As part of the setup process, you should create Modules in Vymo.

## Create Module

A Lead or Partner module can be created in Vymo. To create a new module, login to Vymo Web application.
 
1. Navigate to Customise in the left menu > Select Template. 
2. Select Lead or Partner Template > click Create

<table>
<tr>
<th>Attribute</th>
<th>Description</th>
</tr>
<tr>
<td>Module Name</td>
<td>You can provide a display name for the Module, such as Insurance Agent/ Direct Leads</td>
</tr>
<tr>
<td>Module Code</td>
<td>Auto-generated code, used for internal purposes/ integration purposes</td>
</tr>
</table>

## Edit Module

You can edit the Module details after creation. To edit a Module, navigate to Templates > click edit option > make the edits. Once done, publish the changes and re-login to view the 
updates.
 
## Configure Lead Module

To configure a module, choose a module and begin customization:

<table>
<tr>
<th>Settings</th>
<th>Description</th>
</tr>
<tr>
<td>Module Name</td>
<td>Enter/edit the Module name </td>
</tr>
<tr>
<td>Mobile App</td>
<td>Enter/edit the display labels in FAB button and dashboard</td>
</tr>
<tr>
<td>Module Fields</td>
<td>Customize fields, select which fields should be editable after creation</td>
</tr>
<tr>
<td>Module Dashboard</td>
<td>Customize which dashboard metrics will be available</td>
</tr>
<tr>
<td>Business Profile</td>
<td>Time, Currency and Language preferences</td>
</tr>
<tr>
<td>Allocation</td>
<td>Configure automatic lead allocation rules</td>
</tr>
<tr>
<td>State configuration</td>
<td>Create/modify the states in the business process workflow</td>
</tr>
<tr>
<td>State Transition</td>
<td>Configure the path/transition between different states​</td>
</tr>
</table>

## Configure Partner Module

Configure Partner/Agent on-boarding workflow and activities which will be performed with the Partners.

<table>
<tr>
<th>Settings</th>
<th>Description</th>
</tr>
<tr>
<td>Module Name</td>
<td>Enter/edit the Module name </td>
</tr>
<tr>
<td>Mobile App</td>
<td>Enter/edit the display labels in FAB button and dashboard</td>
</tr>
<tr>
<td>Module Fields</td>
<td>Customize fields, select which fields should be editable after creation</td>
</tr>
<tr>
<td>Module Dashboard</td>
<td>Customize which dashboard metrics will be available</td>
</tr>
<tr>
<td>Business Profile</td>
<td>Time, Currency and Language preferences</td>
</tr>
<tr>
<td>Allocation</td>
<td>Configure automatic lead allocation rules</td>
</tr>
<tr>
<td>State configuration</td>
<td>Create/modify the states in the business process workflow</td>
</tr>
<tr>
<td>State Transition</td>
<td>Configure the path/transition between different states​</td>
</tr>
<tr>
<td>Task/Activity</td>
<td>You can create/ modify activities available in your Partner Relationship workflow.​</td>
</tr>
</table>