# Lead Meetings

Planned and Completed Lead Meetings report shows how many leads were met in a given time frame. This report helps the manager measure the efficiency of their team.

Login to your Vymo Web Application and navigate to Reports > All Reports > Lead Analytics > Planned and Completed Lead Meetings.

These are the metrics in Planned and Completed Lead Meetings report:

<table>
<tr>
<th>Report Column</th>
<th>Description</th>
</tr>
<tr>
<td>Fresh Lead Meeting</td>
<td>Number of leads met for the first time</td>
</tr>
<tr>
<td>Repeat Lead Meetings</td>
<td>Number of leads met again post the first lead meeting</td>
</tr>
<tr>
<td>Leads Created</td>
<td>Number of Leads created</td>
</tr>
<tr>
<td>Live Meetings</td>
<td>
<ul><li>For Future dates: Number of Leads scheduled to meet.</li>
<li>For Past dates: Number of leads scheduled to meet but were not met.</ul></li>
</td>
</tr>
</table>

## Fresh Lead Meetings By Date

This report provides day wise distribution of first meetings with leads for the selected time period. Meetings are listed by meeting date.

## Repeat Lead Meetings By Date

This report provides day wise distribution of repeated meetings with leads for the selected time period. Meetings are listed by meeting date.

## Leads Created By Date

This report provides day wise distribution of number of leads created in the system with a selected time period. Leads are listed by created date.

## Meetings Scheduled

This report provides the number of leads who are/were scheduled to meet by direct and indirect subordinates. Meetings are listed by meeting scheduled date.

## Completed Lead Meetings

This report gives the number of leads with whom fresh/repeat meetings are completed by the direct and indirect subordinates. Meetings are listed by meeting date.

