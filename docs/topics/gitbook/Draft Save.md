# Draft Save

Save incomplete forms for later reference without losing information

You can save your work without entering all the information while creating a Lead. This enables users to pause work with limited information and resume when data is available. 

!!! note ""
    Draft save is available on the Android and iOS app, and is not available on the Vymo Web app.

## Create

Users can enter details and save the Lead or Partner or Activity form before submitting.  

## Update

Users can update the status of either Lead or Partner or Activity form, save as draft and retrieve the draft before submission.

## Access Drafts
 
-  Users can access all the drafts from the Hamburger menu > "Drafts" section.
-  Saved drafts can also be accessed from the Leads List/ Partners List/ Activity List  by clicking the "Drafts" icon on the title bar.
-  If session timeout is configured, Vymo will automatically logout the user, however, the drafts will be retained in the Vymo app and will be accessible by the user when the user logs in again.

## Discard Drafts

Users can delete existing drafts by clicking on the Delete icon in the Draft List view. Drafts can be deleted one by one, you cannot delete drafts in bulk. Drafts will be deleted when user logs out from the app explicitly. Before logout, the user will be asked for a confirmation.

## Invalidate Drafts

If a state update for a Lead or Partner or Activity exists and the user attempts to update the record to another state, the existing draft will be rendered invalid. Hence the user is prompted to retain the older draft and cancel the state update or discard the existing draft to create a new one 

A draft is termed as invalid if the information in the draft is updated in the backend by another user or via  integration, after the draft was created. In this case the draft will be rendered invalid. User will be prompted that the draft is invalid.
