# Lead Funnel

Lead Stage Analysis helps Sales managers to understand the lead funnel. Using this report, a manager can identify the leads in each stage of the Sales cycle, and plan for next actions to improve the pipeline management and conversion.

!!! note ""
    -  The different sections of the funnel represent a different state.They are a part of the sale cycle
    -  Lead funnel starts from Created state and ends in Won state
    -  **new_state** represents the number of leads created
	
Login to your Vymo Web Application> Reports > All Reports > Lead Analytics > Lead Stage Analysis.
 
Hovering over any section of the chart will show respective number of leads and the percentage.
