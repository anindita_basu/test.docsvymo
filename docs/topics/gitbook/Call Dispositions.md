# Call Dispositions

Track Click-to-Call outcomes to gain valuable insights and optimize your sales process.

The Call Disposition feature allows users to record the disposition status after the call, based on the dispositions configured in the system.

## Steps to follow

-    Immediately after a Click-to-call call ends, a Disposition form will pop-up. 
-    Fill in all the details in the form and tap on Update.  

!!! note ""
    The Disposition Form and the statuses can be configured as per your requirements.