# Lead Conversion

This report helps Sales managers derive the number of leads converted per user.

Login in to Vymo Web Application > Reports > All Reports > Lead Analytics > Conversion count by users.

## Purpose of Conversion Count

This report gives Sales managers the reason why the leads were converted/lost by their team members.

## Conversion count by User

<table>
<tr>
<th>Report Column</th>
<th>Description</th>
</tr>
<tr>
<td>Username</td>
<td>Name of Sales team member</td>
</tr>
<tr>
<td>Conversion Count</td>
<td>Number of Leads Won</td>
</tr>
<tr>
<td>Leads Lost</td>
<td>Total number of leads lost</td>
</tr>
<tr>
<td>Total Leads</td>
<td>Total number of leads created in the time frame</td>
</tr>
<tr>
<td>Assignee</td>
<td>Employee ID</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</table>