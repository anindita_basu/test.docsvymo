# Other Activity Report

Other Activity Reports helps monitor the number of activities planned vs. completed by all team members.

<table>
<tr>
<th>Report Column</th>
<th>Description</th>
</tr>
<tr>
<td>User name</td>
<td>Name the employee who directly or indirectly reports to the Sales Manager</td>
</tr>
<tr>
<td>Activity Completed</td>
<td>Number of activities Completed </td>
</tr>
<tr>
<td>Activity Planned</td>
<td>Number of activities Planned </td>
</tr>
<tr>
<td>&lt;Date&gt; Completed</td>
<td>Number of activities Completed completed on the given date</td>
</tr>
<tr>
<td>&lt;Date&gt; Planned</td>
<td>Number of activities Planned completed on the given date</td>
</tr>
</table>

## Activity Prioritisation

Task Prioritisation Report shows the type of activities planned and completed by team members.
 
## Activity Planning

Activity Planning Report shows how many activities are planned/completed on a given date.
