# Win-Loss Analysis

Win/Loss Analysis will aid the manager who's monitoring the sales cycle by measuring the performance of the team in various ways. This will help in deciding the subsequent that can be taken as well.

Let's say, if average meetings conducted is high for the leads who are won compared the leads who were lost, you could derive that your team needs to pursue more meetings before dropping a lead.

Login in to your Vymo Web Application, and navigate to Reports > All Reports > Lead Analytics > Win/Loss Analysis.

!!! note ""
    -  Win/Loss Analysis Metrics includes data of direct and indirect team reports
	-  All the details are filtered based on the date the lead was created

## Win/Loss Analysis Report Details

<table>
<tr>
<th>Report Column</th>
<th>Description</th>
</tr>
<tr>
<td>Average Sales Cycle (Days to win)</td>
<td>Average time taken to call the lead for the first time where lead is won.</td>
</tr>
<tr>
<td>Average of Meetings (Won Leads)</td>
<td>Average number of meetings before the lead was won.</td>
</tr>
<tr>
<td>Win % of Total Opportunities</td>
<td>Percentage of leads created which are won.</td>
</tr>
<tr>
<td>Win Count Per Agent</td>
<td>Average number of leads won per agent.</td>
</tr>
<tr>
<td>Average Sales Cycle (Days to lose)</td>
<td>Average time taken to call the lead for the first time where lead is lost.</td>
</tr>
<tr>
<td>Loss % of Total Opportunities</td>
<td>Percentage of leads created which are lost.</td>
</tr>
<tr>
<td>Loss Count Per Agent</td>
<td>Average number of leads lost per agent.</td>
</tr>
<tr>
<td>Average of Number of Meetings (Lost Leads)</td>
<td>Average number of meetings it took before the lead was lost.</td>
</tr>
</table>