# Vymo Premium Support - SOP

This document (or SOP) is applicable for supporting queries in the English language only (email and phone). For other language support, please talk to the Vymo Sales team.

1.	Dedicated resource(s) who will support end user enquiries starting the Launch date. Dedicated Phone number (with IVR Support) number will be provided. **Note:** This is the only number to be made available to the end users. 
2.	Client SPOC will train the dedicated resources of Vymo Premium Support (VPS - will refer to VPS as Vymo Premium Support going forward), based on a well documented Training Manual. This manual will be the ONLY source of information which will be used to train the VPS team. Anything outside of this manual needs to be called out and refresher training will need to be provided by the Client team. Hence, all the necessary training for the VPS team to be completed before Go Live, and Training Manual to be shared.
3.	VPS will respond to end user enquiries on Vymo Application as well as Customer’s business workflows as long as training signoff is provided by Vymo Support team in conjunction with Vymo Project Team and Client Teams.
4.	All the issues will be handled on a first come, first served basis. **Note:** Users will have to reach out to Vymo Premium Support and dial the number again if the lines are busy.
5.	If the users are unable to reach out to VPS phone no. despite multiple attempts, users can reach out to VPS at support@getvymo.com.
6.	VPS will provide daily reports for the 1st month post signing up for Premium Support. Reports will be sent at the end of the working hours on each of the weekdays. Post the first month (30 days from launch date), VPS will send the report once a week.
7.	Any end user questions that pertain to Client specific queries that VPS is not trained or enabled to handle (e.g. Product info, Data mapping issues that has dependency on Client IT teams etc), will be collated at the end of the day and shared back with the Client SPOC. Client SPOC will be requested to provide all the necessary support immediately to resolve the user queries. VPS will call the users back with the updates as and when it is sent to VPS. VPS will work with the Client SPOC on streamlining these queries and refining the responses for end users (aka Sales agents).
8.	In case the incident is classified as a Change Request aka CR (and not a Bug), the process for new CR will be initiated after mutual agreement between the Client SPOC and VPS team. In this case, VPS will handover the case to Vymo CSM team. Vymo CSM team will get int touch with the Client SPOC to take this forward.
9.	Lead time to setup VPS team would be 8-12 weeks from the time the Client confirms about signing up for the Premium Support model.

## High Severity Incidents (S1 & S2)

1.	In case the Sev1 or Sev2 incident happens outside the VPS Timings, please write to support@getvymo.com and call the First Level Escalation for Sev1 issues. Below escalation matrix can be followed till such time that the Sev1 Incident is acknowledged by anyone in the escalation points in the matrix.
1.	Any security incident reported is automatically classified as Severity 1 and will be acted upon with highest priority. If required, VPS recommends the services to be shutdown for end users till the incident is resolved or patched.

### If the incident has originated at Client end and is being resolved by the Client

1.	Client SPOC to keep the VPS team informed on email and phone call in case there is an Incident Reported at the Client end that can impact the Vymo users.
2.	Client SPOC to share the timelines for resolution along with communication on email and confirmation once the resolution has been achieved.
3.	VPS to communicate to Vymo users on the basis of information given to the VPS team.
4.	RCA to be shared to the Vymo Support Team and the Project Team.

### If the incident is originated at Vymo end and is being resolved by Vymo

1.	VPS will keep the Client SPOC informed of the Severity of the Incident and communicate as per agreed timelines for Resolution and Response.
2.	VPS team will keep phone lines available and keep the Vymo users informed about the issue and update as and when updates as available.
3.	RCA to be shared with the Client SPOC as per timelines.

## Incident Details (For Client SPOC)

Please provide the below mandatory details as a part of the incident reporting. Without these details, it will take more iterations (and hence more time) to respond and resolve the incident. Please note, this only applies when the issue is being identified and reported by the Client SPOC.

1.	Number of user(s) impacted
2.	VYMO Login ID(s) (sample IDs of impacted users)
3.	Incident reported in App (Android or iOS or both) or Web or both.
4.	Detailed Incident Summary & Description
5.	Screenshot(s) & Videos of the incident reported

## Expectations from Customer end

1.	Customer will designate an in house Support team SPOC to engage with Vymo Support team for all discussions on Reporting and Reviews.
2.	In case Client SPOC needs further support in resolving the incident, they can contact VPS by sending an email to support@getvymo.com.

## VSS Timings

### India (UTC+5:30)

<table>
<tr>
<th>Working Days</th>
<th>Timings</th>
</tr>
<tr>
<td>Mon-Fri</td>
<td>9:00AM-6:00PM</td>
</tr>
<tr>
<td>Saturday (1st, 3rd and 5th)</td>
<td>9:00AM-6:00PM</td>
</tr>
<tr>
<td>Saturday (2nd, 4th)</td>
<td>Unavailable</td>
</tr>
<tr>
<td>Sunday</td>
<td>Unavailable</td>
</tr>
<tr>
<td>All Public Holidays</td>
<td>Unavailable</td>
</tr>
</table>

###  UAE/Dubai (UTC+4:00)

<table>
<tr>
<th>Working Days</th>
<th>Timings</th>
</tr>
<tr>
<td>Mon-Thu</td>
<td>9:00AM-6:00PM</td>
</tr>
<tr>
<td>Saturday</td>
<td>9:00AM-6:00PM</td>
</tr>
<tr>
<td>Sunday</td>
<td>9:00AM-6:00PM</td>
</tr>
<tr>
<td>Friday</td>
<td>Unavailable</td>
</tr>
<tr>
<td>All Public Holidays</td>
<td>Unavailable</td>
</tr>
</table>

### Vietnam & Thailand & Indonesia (UTC+7:00)

<table>
<tr>
<th>Working Days</th>
<th>Timings</th>
</tr>
<tr>
<td>Mon-Fri</td>
<td>9:00AM-6:00PM</td>
</tr>
<tr>
<td>Saturday (1st, 3rd and 5th)</td>
<td>9:00AM-6:00PM</td>
</tr>
<tr>
<td>Saturday (2nd, 4th)</td>
<td>Unavailable</td>
</tr>
<tr>
<td>Sunday</td>
<td>Unavailable</td>
</tr>
<tr>
<td>All Public Holidays</td>
<td>Unavailable</td>
</tr>
</table>

## Vymo Severity Definitions

### Severity 1 (Urgent)

-  **Definition:** More than 25% of users impacted.
-  **Examples:** Login Is not working
-  **Action:** <ul><li>From Vymo: Communication from VPS or Customer Support Manager via email and phone call within 5 mins of a Sev1 issue being identified at Vymo.</li><li>From Client: Email & Phone Call from Client SPOC to VPS and Vymo Customer Support Manager.</li><li>Notify the client within 15 mins with issue validation, from the time Vymo's internal monitors detects the same.</li><li>All hands on the deck. Send regular emails to client about the status every 30 min.</li></ul>
-  **Resolution:** <ul><li>Permanent Resolution or acceptable work around, within 4 hours from the time the incident is reported.</li><li>In case of Android App issue, we will provide a fix and make it available on Vymo hosting, so that users can be unblocked immediately.</li><li>For iOS, we use code push mechanism (Requires a stop and start of app to get new code changes), wherever applicable (Generally 50% of issues falls under this). Only in cases we need to go through AppStore submission (For binary builds), the SLA cannot be met and depends on approvals from Apple.</li></ul>
-  **RCA:** RCA should be available and provided to client within 5 business days.

### Severity 2 (High Severity)

-  **Definition:** More than 10% up to 25% of all users impacted for one client.
-  **Examples:** Existing users are able to continue working, however, new logins are impacted.
-  **Action:** <ul><li>From Vymo: Communication from VPS or Customer Support Manager via email and phone call within 15 mins of a Sev2 issue being identified at Vymo.</li><li>From Client: Email & Phone Call from Client SPOC to VPS and Vymo Customer Support Manager. Notify the client within 1 hour with issue validation, from the time Vymo Support gets phone call/email from the client or from the time Vymo's internal monitors detects the same. Send regular emails to client about the status every 4 hours.</li></ul>
-  **Resolution:** <ul><li>Permanent Resolution or acceptable work around, within 16 hours from the time, the incident is reported.</li><li>In case of Android App issue, we will provide a fix and make it available on Vymo hosting, so that users can be unblocked immediately.</li><li>For iOS, we use code push mechanism (Requires a stop and start of app to get new code changes), wherever applicable (Generally 50% of issues falls under this). Only in cases we need to go through AppStore submission (For binary builds), the SLA cannot be met and depends on approvals from Apple.</li></ul>
-  **RCA:** RCA should be available within 5 business days for internal teams.

### Severity 3 (Medium Severity)

-  **Definition:** Less than 10% of users impacted for one client.
-  **Examples:** Edit profile fields not editable
-  **Action:** <ul><li>From Vymo: Near realtime responses back to the end users with a reasonable amount of hold time on the call or a callback in case of any issues that is taking a few additional minutes to investigate. Callback will be made in less than 30 minutes in such cases.</li><li>From Client: End user phone call to the VPS team as and when issues occur. Notify the client in the Tracker with the details of the issue reported by the user(s). Client will be provided with the release date based on the effort in case of any bug fix.</li></ul>
-  **Resolution:** <ul><li>Resolution Times:The responses for the S3 issues will be provided on a real time basis for the ones that do not need escalation to L2/L3 teams. (e.g. App installation, Login related issues, and basic workflow related questions as part of the current levels of deployment).</li><li>Response Times: The response times for S3 incidents will be near real time (Assuming users have to retry when the lines are busy serving customers). These responses will be logged in our Internal Ticketing Tool Freshdesk within 1 hour of the end of the call with the user.</li></ul>
-  **RCA:** Sev 3 incidents with a recurrence of more than 5 times a week, need to be provided a RCA within 10 days.

### Severity 4 (Low Severity)

-  **Definition:** Less than 1% of users impacted for one client.
-  **Examples:** Adoption report is not showing data for older than 30 days
-  **Action:** <ul><li>From Vymo: Near realtime responses back to the end users with a reasonable amount of hold time on the call or a callback in case of any issues that is taking a few additional minutes to investigate. Callback will be made in less than 30 minutes in such cases.</li><li>From Client: End user phone call to the VPS team as and when issues occur. Notify the client in the Tracker with the details of the issue reported by the user(s). Client will be provided with the release date based on the effort in case of any bug fix.</li></ul>
-  **Resolution:** <ul><li>Resolution Times: The responses for the S4 issues will be provided on a real time basis for the ones that do not need escalation to L2/L3 teams. (e.g. App installation, Login related issues, and basic workflow related questions as part of the current levels of deployment)</li><li>Response Times: The response times for S4 incidents will be near real time (Assuming users have to retry when the lines are busy serving customers). These responses will be logged in our Internal Ticketing Tool Freshdesk within 1 hour of the end of the call with the user.</li></ul>
-  **RCA:** NA
