# Workflow and States

Any business process that involves a series of steps can be configured as a workflow in Vymo. You can create the following business processes, not limited to:

-  Lead to Opportunity
-  Lead to Customer
-  Customer on-boarding
-  Service ticket management

## States

States are the milestones in a business process workflow. You can configure Start, End and Interim milestones. Each workflow will have (1) Start state and (2) End states (Win & Loss) by default. In addition to these, any number of interim states can be configured in the workflow.

!!! note "Orphan states"
    It is important to note that as you add/remove Lead states, you may inadvertently create  states that are not connected to any other states. These states, called orphan states, will remain active in the system and any Leads in these states cannot be moved to any other state until the configuration is updated.

## State transitions

A State transition describes which States can transition to which other States. For instance, in the above workflow you can move from New Lead to Schedule Meeting but not to ID Verification. 
Data entered in each State is carried forward through the Transition into subsequent states. This enables the user to always have access to all data entered against the Lead through the workflow.

## State tags

Each State has a State tag associated with for status reporting. Below are the list of valid State Tags:

-  Meeting State
-  Open State
-  Met State
-  Unmet state
  
## Input Fields

You can configure which input fields should be added in the form to be captured during lead acquisition/ service management. A wide variety of field types are supported in Vymo.

Each state has fields that are defined as part of the module configuration.
Different types of field types:

-  Labels, 
-  Text box
-  Date/Time 
-  Lists

Read more in the Input Fields section.

## Field tags

Similar to state tags, Field tags define properties of a field. For example, a location field can capture the Home or Office location of the Lead, such an address can then be tagged as Home or Office location.

## Limits Allowed

Count allowed in each step of workflow configuration:

<table>
<tr>
<th>Module</th>
<th>Total allowed</th>
</tr>
<tr>
<td>States in a Lead workflow</td>
<td>255</td>
</tr>
<tr>
<td>State transitions in a Lead workflow</td>
<td>999</td>
</tr>
<tr>
<td>Fields in a single State</td>
<td>255</td>
</tr>
<tr>
<td>State Tags</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Field Tags</td>
<td>&nbsp;</td>
</tr>
</table>

## Module - Role/Access Settings

You can configure who can do, in the module settings.

