# Release Management

Configurations done using Vymo Admin Console will be saved as a draft.

You should manually Release the changes to Staging/Sandbox environment for UAT/Business testing purpose. 

Discard will help the user to discard all the non-released changes from the Vymo Admin Console. 
