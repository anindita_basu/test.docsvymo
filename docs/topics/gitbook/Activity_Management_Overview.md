# Overview

As a sales representative, you make sure to check the calendar for meetings you for the day. You can plan your day based on the appointments and the notes if any that you have or want to include in your appointment.

Here's what you can do when with Activities in Vymo:

-  Create activities and follow-up tasks
-  Search for and view tasks and appointments
-  View reports and analytics information
-  Manage your calendar
-  View reports and analytics information
-  View overdue tasks

## Create Activity

You can schedule an activity in the future or you can log a task done earlier.

1.	Schedule Activity
2.	Log Activity

You can create Tasks or Events in your activities list. Activities created by/for the Sales agents/users can be of the following types:

-  Task
-  Call
-  Business planning 
-  Business Review meeting
-  Cadence Call 
-  Send Email
... and any other type of activity required in your business process

All activities planned by a user or assigned to the user will be visible in the user's Calendar in the Vymo app. The users can provide information about how the meeting went.

Here's how you can create activities:

Login to the Vymo mobile application > click on the (FAB) floating action (+) button > Schedule/Log Activity > enter the activity details. 
 
You can provide the following details while creating an activity:

-  Lead/Opportunity/Partner to meet or speak with
-  Date and time of the meeting
-  Location (you can add the address details from the maps)
-  Participants: Team members, Managers or Customer contacts on the meeting -  Subject and Description of the meeting

## Configure Activities

You can configure the following: 

-  Type of Activities allowed
-  List of Modules (Lead, Opportunity, Partners etc.)
-  Details to be captured while creating a meeting
  
Contact Vymo Support to help you with the configuration.

## Activity States

There are 4 states an Activity can go through:

<table>
<tr>
<th>Activity State</th>
<th>Description</th>
</tr>
<tr>
<td>Planned</td>
<td>User creates a new activity</td>
</tr>
<tr>
<td>Canceled</td>
<td>Activity is canceled (say, if the lead is not available in office)</td>
</tr>
<tr>
<td>Rescheduled</td>
<td>A meeting is rescheduled</td>
</tr>
<tr>
<td>Completed</td>
<td>Activity is completed</td>
</tr>
</table>

## Activity Detection

Vymo can detect and log business activities of your Sales agents/users. This cuts down the time spent on updating the details in the system. The following activities are detected by the user are:

-  Calls: Calls made/received from a Lead or Partner from within the Vymo mobile app
-  Meetings: Meetings with a Lead or Partner

!!! note ""
    If you are using Microsoft Office 365 Outlook, you can sync your calendar with the Vymo Calendar. Read more about the data sync.
