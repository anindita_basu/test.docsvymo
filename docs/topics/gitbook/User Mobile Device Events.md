# User Mobile Device Events

User Mobile Device Events Report provides details of the user's mobile device.  This allows admins & managers to track whitelisted users' mobile details and detect any suspicious login activity.

Login to your Vymo Web Application > Reports > All Reports > User Reports > User Mobile Device Events Report.

User Mobile Device Events Report works on a per user basis; you can generate the report on a per-user basis.

## User Mobile Device Events Report Metrics

The details in the User Mobile Device Events Report are:

<table>
<tr>
<th>Report Column</th>
<th>Description</th>
</tr>
<tr>
<td>User Code</td>
<td>User ID of the user who's report is being generated</td>
</tr>
<tr>
<td>User Name</td>
<td>Name of the user whose report is being generated</td>
</tr>
<tr>
<td>OS Name</td>
<td>Type of OS - Android/iOS</td>
</tr>
<tr>
<td>OS Version</td>
<td>Version number of the OS</td>
</tr>
<tr>
<td>App Version</td>
<td>Vymo app version number which is installed in the user's mobile device at the time of login</td>
</tr>
<tr>
<td>Device Manufacturer</td>
<td>Manufacturer's name</td>
</tr>
<tr>
<td>Device Model</td>
<td>Mobile device model name</td>
</tr>
<tr>
<td>Last Seen</td>
<td>Last time when the user performed any activity on Vymo app in the phone</td>
</tr>
<tr>
<td>IMEI1</td>
<td>IMEI1 number (for Android 9 or lower versions)</td>
</tr>
<tr>
<td>IMEI2</td>
<td>If the user's phone supports Dual SIM, IMEI2 will be available</td>
</tr>
<tr>
<td>Device ID</td>
<td>Android_ID (for Android 10 or higher versions)/ UDID (for iOS devices)</td>
</tr>
</table>

!!! note "Note Points"
    **Android**
	-  For Android 10 & higher versions, IMEI number is not available due to security restrictions applied by Android. Hence, only Device ID will be captured for Android users using Android 10 & above. For more details on android restrictions, click [here](https://developer.android.com/about/versions/10/privacy/changes#non-resettable-device-ids).
	-  If the OS version of the android phone is updated, the Device ID will change. Hence, in this case, if the user uninstalls the current Vymo app and installs it again from the PlayStore, the Device ID captured by Vymo app will change and the user will be required to whitelist again (there are no issues expected if the user updates the existing Vymo app).
	-  If the phone is factory reset, the Device ID will change. The user will be required to whitelist again.
	-  Device ID is not completely proven to be unique. Multiple mobile devices can have the same Device ID in case if there is a manufacturing fault.
	
	**iOS**
	-  For iOS devices, UDID is captured instead of IMEI number due to restrictions applied by Apple.

