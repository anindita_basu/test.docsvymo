# Lead Import

You can import leads in bulk into Vymo.

If you have to import a number of leads at once, you can use the Lead Bulk Import functionality. 

!!! note ""
    Users who have access to Vymo Web application can import leads in bulk. 
	Bulk import for Leads is not available on the Mobile platform.

## How to Import Leads

In order to import, login to the Vymo Web application > navigate to Leads > Leads List > Bulk Import > select a file > Upload. Multiple users can upload leads at the same time.

### Prerequisites

1.	Files in .xls/.xlsx/.csv formats will only be allowed to be imported.
2.	Maximum file size allowed is 100MB.
3.	As per an internal benchmark, 100000 leads can be imported at once
4.	If you import the same file or leads list, duplicates will be ignored.
5.	Localization (support for different locale) is not supported as part of bulk import.

### After Import

1.	Once the import process is complete, the original file (uploaded) and the summary file will be available for download.
2.	The summary file will be sent via email as well.
3.	The data will be imported as Leads in your Sales Leads list. 
