# Calendar

View meetings planned for the week, plan for the day based on upcoming sales meetings/ service delivery tasks.

You can view the list of meetings with the Leads/ Partners on the Calendar. You can view your calendars by day, week or month. You can view the status of the meetings planned and completed. 

iOS

-  Calendar shows a Month View
-  Calendar starts from last Sunday

Android

-  Calendar shows a Week view
-  Calendar starts from yesterday

## Planned

Tapping on the planned count, shows all the activities & lead meetings that are planned, completed or logged in the given date range

## Completed

Tapping on the completed count, shows all the activities & lead meetings that were completed or logged in the given date range.
 
Read about how you can plan for the meetings for the day with Vymo's [Route](Best Route.md) and [Nearby](Nearby.md) features.
