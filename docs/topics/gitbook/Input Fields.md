# Input Fields

Use any field type you need to create custom form fields. Everything from text fields to date pickers to check boxes are at your disposal.

-  You can configure list of Input fields which will be required to be captured in application forms. 
-  These forms and fields will show up when your Sales Agents/reps are meeting prospects and creating Leads/ Partners / Activities.   You can configure whether the field is mandatory or not
-  Field name will help identify the fields uniquely 
-  App display label is used to display the actual name of the field which will appear in web app / mobile app. 

Vymo supports the following field types:

<table>
<tr>
<th>Attribute</th>
<th>Description</th>
</tr>
<tr>
<td>Text field</td>
<td>Capture simple text</td>
</tr>
<tr>
<td>Sentence</td>
<td>Capture description</td>
</tr>
<tr>
<td>Number</td>
<td>Enter numbers</td>
</tr>
<tr>
<td>Decimal</td>
<td>Enter numbers with decimal point</td>
</tr>
<tr>
<td>Dropdown</td>
<td>Single-select, Multi-select, Decision fields (conditional fields)</td>
</tr>
<tr>
<td>Location</td>
<td>Capture location of the lead</td>
</tr>
<tr>
<td>Currency</td>
<td>Capture a price value</td>
</tr>
<tr>
<td>Meeting</td>
<td>Capture a meeting Meeting can have a location or not.</td>
</tr>
</table>

## Dropdown input field

Used to provide a predefined set of values in the data input form. Vymo supports the following types of the dropdown input fields:

<table>
<tr>
<th>Type of field</th>
<th>Description</th>
</tr>
<tr>
<td>Simple Dropdown</td>
<td>List of values available for selection, such as Country</td>
</tr>
<tr>
<td>Nested/ Dependent Dropdown</td>
<td>Display list of fields when user selects a specific value from the dropdown. You can only link an existing dropdown to an option of the dropdown. Make sure that you have created the all the dependent input fields before creating a dependent dropdown.</td>
</tr>
<tr>
<td>Multi-select dropdown</td>
<td>User can select multiple values from the dropdown in the form</td>
</tr>
</table>

## Meeting input field

-  Create meetings with Leads/Partners
-  You can capture 
    -  Date & Time of the meeting
    -  Duration of the meeting
    -  Location where the meeting will be held
 
## Location input field

-  The location input will provide a map picker in the mobile and web user form
-  The location field will allow the user to search for a location or pick a location from the Google map
-  The latitude and longitude of the location picked from the location input field will be stored
  
## Geo-coded address field

In order for Vymo to represent the location in the user’s map and provide intelligent recommendations, these have to be converted to latitude and longitude format.

-  There are some pre-defined text address fields that can be used instead of the location input field.
-  You can enter the complete address field and Vymo will use it as a normal location field.
Contact Vymo Support to configure the list of address fields.

## FAQ

**Can I delete an input after creating it from the field configuration?**

No, input fields cannot be deleted, they can only be disabled. Once disabled, the input fields will not be displayed in user facing data input forms.
