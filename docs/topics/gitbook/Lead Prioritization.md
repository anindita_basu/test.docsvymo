# Lead Prioritization

Leads can be prioritised based on rules, define prioritisation labels and plan sales activity with leads which matter the most.

Though teams have data (and intuition) to highlight which leads are better than others, sales teams struggle to effectively prioritize good leads over bad ones. It doesn’t matter if your team is a master at generating sales leads if you don’t separate them into categories.

With Vymo's Lead Prioritization capability, users can look at the list and identify the priority of the Leads/Partners and plan for next steps.

## Configure Lead Priority

You can configure sorting of leads/partners based on Priority rules and tag them, for example: 

-  High, Medium, Low 
-  Hot, Warm, Cold
-  Yes or No

!!! note ""
    To configure custom prioritization rules, contact Vymo Support.

## View Lead Priority

You can view the priority for the leads/partners in the web and mobile platforms:

-  Sorting leads based on Priority (High, Medium, Low) or (Hot, Warm, Cold) is possible, contact Vymo Support to configure this.
-  You can filter records based on a specific priority, contact Vymo Support to help you out.
-  Records that are not prioritised will not have any visual indicator

-  Web: You can login to Vymo Web application > navigate to Leads > Leads list and view the priority assigned.
-  Android: You can login to Vymo Mobile application > navigate to Leads list and view the priority assigned.
-  iOS: You can login to Vymo Mobile application > navigate to Leads list and view the priority assigned.