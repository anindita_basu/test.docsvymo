# Count of Users Billed

This report enables the Admin to calculate the number of users to be billed for, in a given period of time.

Login to your Vymo Web Application > Reports > All Reports > User Reports > Number of User Billed.

## Users Billed Report

The details in the Billing Report are:

<table>
<tr>
<th>Report Column</th>
<th>Description</th>
</tr>
<tr>
<td>Active users</td>
<td>Total number of users for which the client will be billed</td>
</tr>
<tr>
<td>Licensed users</td>
<td>Total number of contracted users</td>
</tr>
<tr>
<td>Date</td>
<td>Date when the maximum users were enabled</td>
</tr>
</table>
