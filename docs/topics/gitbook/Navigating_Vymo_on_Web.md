# Navigating Vymo on Web

Access Vymo on Web platform, designed for Sales Leaders and Management professionals for better visibility into Sales lifecycle and ability to analyse reports derive insights.

## Leads 

### Cover 

This is where you land when you log in. The Leads Cover page is a dashboard that gives you visibility into the sales pipeline, leads in different stages, etc. The cards shown on this 
page can be customized.

-  Hover over the Information icon on each card to view a description.
-  Notice the Filter button on the right. This allows you to filter results either for yourself, your entire team or a member of your team. You can also select a date range and/or compare results across months.


### Productivity

The Productivity page gives you a graph view of lead workflow states and activities, which shows you how productive your sales team is.

You can select different parameters for the X and Y axis (from the drop-down box) to view specific results. 

### Leads / Opportunities List

This is similar to the View Leads screen on the mobile app. You can view and manage all your Leads here. You can also see in which sales stage the Leads are, who they are assigned to, etc.

-  You can click the Add button to add leads. 
-  You can also select a Lead and reassign it to someone else on your team by clicking the Reassign button. 
-  You can download the entire list of leads in CSV format or bulk upload Leads using an upload template. 
-  You can use the Filter button on the right to filter and search for specific Lead details.
-  You can click on a Lead row to view and edit all the details associated with that lead in one place.
  
## Customers 

Since Customers are a type of Partner in Vymo, let’s skip straight to Partners. 
  
## Partners

### Cover 

The cover page is similar to the Leads cover page and gives you a bird’s eye view of all partner related activities and coverage. 

### Partners List

From this page, you can view and manage all the Partners present in your system. You can view who in your team is assigned to a Partner and which tier a partner falls into.

-  Similar to Leads, you can reassign partners, download the partner list or bulk upload partners from this page.
-  Click on any Partner to view all associated details. You can also schedule and log Partner activities from the details page.
-  You can also see the Business Metrics available for partners inside partner details. These metrics shows the performance of your partners' business based on the metrics configured by you.
  
## Activities Cover 

The activities cover page shows you the total number of planned activities, the closure rate, etc. The cards shown on this page can be customized. 

### Productivity 

The productivity page gives you visual depiction of the activities of your team along with a summary of the progress, that is, how many activities were created, rescheduled, completed or cancelled. The actions on activities are configurable.

-  You can click on the Y Axis drop-down box to view details of the states or the type of activities. 
-  You can also download the activities report directly from this screen.
 
### Activities List 

The activities list page lists all the meetings by team members assigned to it, along with the type of activity and it also tells you how many times an activity was rescheduled.

Here you can also schedule new meetings/activities for future planning and log activities that are completed or cancelled. Similar to Leads and Partners, you can reassign activities and download the full list as an excel sheet.
  
## Settings
 
### Manage Users

-  Click the Add button to add new users. 
-  Click the Alert button to send a notification. For example, if your team member has not logged in for a while, you can send an SMS to him/her by selecting the SMS option and clicking alert.
-  You can also send bulk notifications to all users by clicking this button. 
-  Similarly, you can bulk upload or download user lists. 
-  Click on any user to view and edit user details.
-  From this page, you can unlock a user account that's been blocked on the mobile app due to multiple incorrect password tries. 
-  You can also disable or enable a user and change the Manager.
  
## Geo & Adoption

Here you can view and track all users’ daily field sales activities.

### Adoption & Usage 

Adoption & Usage shows you how many users are active, how many logged in today, the Daily Average Usage, etc.

If you scroll down, you also view their engagement and activity details.

!!! note ""
    -  Engaged Users are users who are logged in and performing actions in the Lead, Partner or Activities module.
    -  Active Users are those who are logged in to Vymo but are only viewing the different sections in Vymo, without making any changes to the Lead, Partner or Activities module.

### Geo

The Geo page shows location details such as Last Location, Location History, Coverage and Time Spent. Here you can see the last known location of the user/self.

-  Next, click Location History. 
-  Select the user and duration. You can only view details for the past week, no longer. 
-  Notice the color coding for each day. You can view the start and end location of all meetings and the route taken. 
-  If you hover over the number pins, you can view details of logged meetings.

Coverage shows you where your team is spending the most time.

Time Spent shows you how many times the user met with a partner or lead and the minutes per meeting. The minutes are calculated based on the geo-fencing radius configured in Vymo.

Click the Download button at the bottom of this tab, to download these details.

### Downloads

Downloaded reports get saved here, if configured that way.

### Travel Summary

You can view a summary of the total distance traveled by all the users in a particular day. 
  
## Reports

From the Reports section you can view all the standard reports on Sales and User performance. Let’s take a closer look at one of the reports. 

-  Click Task Distribution to view the type of activities planned and completed by the team and yourself on a given date.
-  The first report shows you the breakdown of planned activities.
-  The second report shows you the type and number of activities completed. 
 
!!! note ""
    Activities and Tasks mean the same thing in Vymo.

## Logout 

To log out of the Vymo Web Platform, click the drop-down button next to your User ID on the top-right corner, and click Logout.
 
