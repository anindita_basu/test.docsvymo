# User Call Report

Analyze various metrics on daily calls made through Click-to-call to help your team

The User Call Report is available for Admins to view daily trends on calls and take necessary steps to improve performance. You can track various metrics, such as call duration, call volume across teams, etc.

!!! note ""
    By default, the call recordings are only available for Admins & Managers. However, it can be made available for specific user roles based on customer requirements.

Navigate to the Reports section and click on User Call Reports. You can view the following details:

-  Average number of calls made
-  Average call duration
-  Call completion rate (calls connected/total calls made)
-  Visual representation of all the calls made each day, classified by call status - Completed, Busy, No Answer, or Failed. 

You can also view calls by the hour. 

-  Click the filter button on the top-right corner
-  Scroll down to the Plot By drop-down box and select Hour. 
-  Click Apply Filters and you can view the hourly breakup of calls per day.

Scroll down to view the details of each call, including the recordings of completed calls.

You can also download the report by clicking the Download Report button. 

You can view the same report on mobile as well.