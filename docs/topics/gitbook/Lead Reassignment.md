# Lead Reassignment

Leads will be assigned based on the allocation rules setup. In case, team Managers or Supervisors want to reassign the leads among users to manage their team's bandwidth, they can use the Lead Reassignment option. This will override Vymo's default lead allocation.

You can re-assign leads from the Vymo's web and mobile apps.
 
## Web

Login to the Vymo application on the web, navigate to Leads List.

Select a Lead and choose Re-assign option from the actions menu.
 
## Mobile

Login to the Vymo Mobile app > navigate to Leads list and select a Lead > then select the Reassign option. 