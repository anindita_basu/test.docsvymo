# Lead Configuration

In Lead configuration, you can configure the Lead Dashboard, and Lead module level settings.

## Lead Menu Settings

You can configure the sub-menu options to be displayed and the sequence in which it should be displayed in the Leads Module menu:

-  Dashboard
-  Allocation
-  Funnel
-  Leads list
-  Productivity

## Lead Module Profile Settings

You can provide the Lead level profile settings:

<table>
<tr>
<th>Attribute</th>
<th>Description</th>
</tr>
<tr>
<td>Locale</td>
<td>Select a locale for the Lead module 
If multiple languages are supported in your app, then on changing the language, the app will reflect the new language
</td>
</tr>
<tr>
<td>Country</td>
<td>Country of incorporation, where your business is registered</td>
</tr>
<tr>
<td>Timezone</td>
<td>Timezone in which you are operating, all the date and time fields will display information in this timezone.</td>
</tr>
<tr>
<td>Currency</td>
<td>Currency you are operating in, all the price related fields will show this currency.
  Reports will be displayed in this currency
</td>
</tr>
<tr>
<td>Currency ISO</td>
<td>ISO Code of the currency selected</td>
</tr>
<tr>
<td>Quantity</td>
<td>Select a unit of measurement to be displayed for this module</td>
</tr>
<tr>
<td>Country Calling Code                           
</td>
<td>Select a country code for accepting phone numbers.
The country code selected here will be added as a prefix for all phone number fields.
  While initiating calls from your phone, Vymo will add this as the default country code 
</td>
</tr>
</table>