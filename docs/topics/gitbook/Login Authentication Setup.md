# Login Authentication Setup

A connected app setup will require the following:

1.	SSO Configuration - in absence of Salesforce SSO
2.	Connected app setup - Salesforce Classic/Lightning Edition

The below topic covers the SSO configuration with Salesforce via Azure AD. 

## OAuth via Azure AD

You can set up authentication via Microsoft Azure Active Directory (AD). Refer to [this link](https://docs.microsoft.com/en-us/azure/active-directory/saas-apps/salesforce-sandbox-tutorial) to follow the steps involved to setup authentication. This is how the SAML Configuration looks like in Azure:
 
## Salesforce Classic Edition

If you are using the Salesforce Classic Edition, you can follow the steps mentioned below:

Step 1: Click Settings.

Step 2: Click Manage applications.

Step 3: Navigate to Connected Application section > Click New.

Step 4: Configure the New App.

-  Enter the Connected App, API name, Contact Email address 
-  Enable the “Enable OAUTH settings” option
-  Configure the callback URL as specified below

Callback URL will differ according to the environment:

STAGING: https://staging.lms.getvymo.com/sso/authenticate/sf

PRODUCTION: https://<CLIENT>.lms.getvymo.com/sso/authenticate/sf
 
Step 5: After the details are entered, click Save.
 
Once configured, you can share the details with Vymo Support team:

-  Consumer Key 
-  Consumer Secret
-  Salesforce Organization Id
-  Domain name with the instanceURL

To create a domain, go to Salesforce Domain > Setup > Domain Management > My Domain > New (eg: https://<domain-name>.cs75.my.salesforce.com) 

## Salesforce Lightning Edition

If you are using the Salesforce Lightning Edition, you can follow the steps mentioned below:

Step 1: Login to Salesforce and select Setup.
 
Step 2:  Go to Apps > App Manager > click on New Connected App.
 
Step 3: Configure the New App.

-  Connected App, API name, Contact Email address 
-  Enable “Enable OAUTH settings”
-  Configure the callback URL as specified below

Callback URL will be differ according to the environment

STAGING: https://staging.lms.getvymo.com/sso/authenticate/sf 

PRODUCTION: https://<CLIENT>.lms.getvymo.com/sso/authenticate/sf 

Once saved, you can see the App Summary page as below:
 
Step 4: Go to Apps > App Manager > Select the app created > Select View.

Once configured, you can share these details with Vymo Support team:

-  Consumer Key 
-  Consumer Secret
-  Salesforce Organization Id
-  Domain name with the instanceURL

To create a domain, go to Salesforce Domain > Setup > Domain Management > My Domain > New (eg: https://vymoad--HussainSB.cs75.my.salesforce.com)
