# Suggestions

Nudge your users to creatively work towards better sales activities, for example, if a Lead has rescheduled a meeting, you can plan for a meeting with another Lead based on availability.

Vymo processes Sales activity data and suggests important business activities. Vymo uses the engagement data points to suggest what other tasks the Sales agents can do to improve sales performance or productivity.

!!! note ""
    To enable Suggestions on your Vymo account, contact Vymo Support and we will help you out.

-  Vymo processes the history of Activity data 
-  Vymo also looks for free slots in the calendar
-  Based on the data points analysed, Vymo suggests you to engage with a partner, say call a partner, schedule a call, plan new product training or review ad-sales revenue of a partner: 
    -  Call 
    -  Meeting 
    -  Reminders to complete an action 
    -  Alerts to clear backlog 

## Accept Suggestion

Users can choose to accept a suggestion, it will be added in the calendar.
 

## Decline Suggestion

If users choose to decline the suggestion, users can provide a reason why the suggestion did not work or if the suggestion was not relevant.
