# Integrating Work Calendar

Integrate Google calendar into Vymo calendar for an unified view of calendar items.

Companies with an existing work email such as Microsoft Office 365 or Google G-Suite need a unified view of their calendar across Vymo & the existing calendaring system. By providing an integrated user experience, Vymo delivers a single unified  calendar view for the end users spanning Vymo and existing company-specific calendaring system.

Through this integration Vymo supports Google calendars - both G-Suite and  Gmail. In a future release, Vymo will also support connecting the Office 365 Outlook calendar.  Note that Only 1 calendar provider can be connected per user: Google G-Suite or Office 365 Outlook.

When the user connects their Work calendar to Vymo, Vymo helps the user:

1.	Plan their Vymo activities around their work calendar.
2.	See a unified view of meetings across their work calendar & Vymo calendar.
3.	Avoid overlapping & conflicting appointments.

Users can connect their Google calendar from within the Vymo calendar view.
 
Integration with Google calendar is bi-directional, which means that users can see their Google calendar items on the Vymo calendar and see Vymo calendar items on the Google calendar. Few things to note about the bi-directional sync:

-  Any edits in Work calendar are ignored in Vymo and vice-versa (including drag-drop edits).
-  All Vymo calendar edits are pushed immediately to work calendar.
-  Only Vymo Activities are sent to the Google calendar; Lead meetings are not sent because these are not Activities.
-  Only 1 calendar can be connected as there is only 1 primary work calendar. 
  
## Known issues

-  Each user is allowed to connect only 1 calendar: G-Suite or GMail or any other.
-  Users will need to disconnect an existing connected calendar before connecting another calendar.
-  If the user renamed their Google calendar,  then the calendar  items are not displayed in Vymo calendar even though the account is connected.
-  It could take up to 5 minutes from the time the user connects the Google calendar, for the calendar items to appear in Vymo calendar.
-  External work calendar items are not considered Vymo activities and hence are not counted in any  activity reports. 
-  Users cannot edit Google calendar items in Vymo.
-  Synchronization is one-way: Google items appear in Vymo. Vymo items do not appear in the Google calendar.
