# Activity Efficiency

Activity Efficiency report shows the efficiency of the team. The report includes information such as the tasks completed by a Sales rep in a day or the number of reschedules per activity.

Login to your Vymo Web Application > R	Activity Efficiency.

## Activity Efficiency Report 

The details in this report are:

<table>
<tr>
<th>Report Column</th>
<th>Description</th>
</tr>
<tr>
<td>Reschedules/ Total Activities</td>
<td>Ratio of activities rescheduled vs. total number activities created</td>
</tr>
<tr>
<td>Sales Executives</td>
<td>Total number of direct/indirect team members of the Sales Manager viewing the report</td>
</tr>
<tr>
<td>Task/Rep</td>
<td>Ratio of number of tasks completed by total number of employees reporting to the Sales Manager viewing the report</td>
</tr>
<tr>
<td>Task/Rep/Day</td>
<td>Ratio of Task/Rep by number of days</td>
</tr>
<tr>
<td>Last updated time task</td>
<td>Date on which the task was completed</td>
</tr>
</table>

## Activity/Rep/Day

This report shows the average of the number of tasks completed by a Sales rep per day against the date the task was completed

