# Activity Planning

Activity Planning Report shows how different teams are performing. You can view planned v/s completed tasks in a given period of time.

Login to your Vymo Web Application> R Activity/Task Planning Report.

Following reports are available in this dashboard.

## Planned Activities Over Time

This report shows the number of tasks created on a given date. 

## Completed Activities Over Time

This report shows the tasks that were completed on a given date.
 
## Activity Planning Report

This report shows the Planned task and completed activity against each user by user name.
