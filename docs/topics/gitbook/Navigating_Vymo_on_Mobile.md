# Navigating Vymo on Mobile
  
Vymo on Mobile is available for your sales agents or reps to view the leads or partners or contacts. Your users can also view Sales activities, meetings with the prospects while on the field.

## Hello Screen

Once you log into the mobile app, you land on the Hello screen. The Hello Screen displays information related to all the modules in Vymo, such as, Leads, Partners and Activities. It also allows you to navigate to other parts of Vymo.
Vymo on Mobile is available for your sales agentsorreps to view the leadsorpartnersor contacts. Your users can also view Sales activities, meetings with the prospects while on the field. 

On the top left is the Hamburger Menu, which allows you to navigate to certain sections within Vymo and edit your settings, we will explore this later.
 
On the top-right corner, you can see the Filter and Search buttons.

Filter allows you to view either your activities or your team's activities.

Search allows you to quickly pull up information.

On the bottom-right corner is the Fab button, which allows you to quickly input different types of information.

Each section on the Hello Screen is called a Card. You can navigate to the list of your leads or customers through these cards. The cards on display can be customized. 

Let’s take a closer look at each card.
  
## Backlogs

The first card is Backlogs. Backlogs are past due activities and lead meetings. Scheduled meetings that are not logged move into backlog at midnight on the due date.

### To view your backlogs:

1.  Click VIEW to open and view your backlogs.
1.  To filter the results, click the funnel icon on the top-right corner.
1.  You can view LeadsorPartners assigned to you or assigned to your team.
1.  You can also provide a date range.
1.  Once you’re done, click APPLY and view the filtered results.

### Resolve Backlogs

1.  To resolve a backlog, first click on it. 
1.  On the Details screen, click the Edit button on the bottom right. 
1.  You can choose the appropriate action, add comments, etc. 
1.  Once you’re done, click Update and the backlog goes away.

## Calendar

The calendar card shows you the number of meetings you have planned for the day, meetings you have completed today, and the next scheduled meeting.

Click the card, to view the details of your meetings. 

On the Calendar screen, click the Map icon. There are two tabs here.

-  The Route tab shows the best routes from your location to all your planned meetings today. 
-  Click the Nearby tab to view other Leads and Partners in the area. You can see that a Partner, Edna Chavez is nearby. 

Click the back arrow to go back to the Hello Screen.

## Suggestions

This is the Suggestions card. You can scroll left or right to view all the suggestions and you may call, accept or ignore suggestions with a single click.
Notice that Vymo is suggesting that you schedule a meeting with Edna. Vymo suggests meetings based on your location and schedule. The Suggestions feature is intelligent andoror rule-based. This can be configured as your business rules.

## Leads

The Leads card gives you a quick view of all the Leads assigned to you (and your team).

!!! note ""
    -  Leads can be prioritized based on business rules.
    -  Prioritization distinguishes high value opportunities.
    -  Flexible color & tagging. For example in the figure below, we have: Hot | Warm | Cold
 
-  Click View All to see the full list of Leads assigned to you. 
Here, you can also view Drafts (drafts are created when you save a form without submitting), filter the Leads list, or search for a specific lead. 
-  Click on a Lead to view contact details and history. 
-  Click again on the Lead and you can view all the information associated with that Lead in one place.
 
## Partners

The next card is Partners.

!!! note ""
    -  Partners can be assigned Tiers based on their importance.
    -  Tiers determine the level of engagement & outcomes.
    -  Limited to 5 Tiers : Platinum | Gold | Silver | Bronze | None
 
Here you can see three tiers have been configured. 

-  Click View All to see the full list of Partners assigned to you. 
-  You can drill down to view all Partner related information, such as, Business Trends, Links to this Partner, etc. 
-  You can also view Business and Engagement Details.

Take your time and get comfortable navigating to all the details. When you’re done, click the back arrow till you hit the Hello Screen.

## Customers

Next is the Customers card. Navigating this card is similar to Partners.

!!! note "Please note"
    All business entities such as Customers, Contacts, Merchants, etc. are called as Partners in Vymo. Different types of Partners can be configured depending on the requirement. 

!!! note "Please note"
    Leads & Partner cards can be configured as per needs. You can have multiple leads or partners cards configured based on your business needs. 
 
## Activities

The Activities card shows you your monthly activities.

Click View All to view both Scheduled and Logged activities. 

Here you can see two activities: the first one is logged as Completed; the second activity is scheduled and is marked Open. 

-  Click the open activity to edit it. 
-  Click the Edit icon on the bottom-right corner. You can either complete, re-schedule or cancel open activities.

### Update Activities

1.  Click on the activity you want to edit. 
1.  On the Details screen, click Reschedule. 
1.  Edit the activity details and click Update. 

You can see that the edit is logged in the Activity History and the activity has been rescheduled. 
 
Go back to the Home screen and scroll down to Targets.

## Targets and KRAs

The Targets card shows your monthly targets and the progress you’ve made so far.
The KRAs card shows you your KRAs for Leads, Partners, Customers and Activities. 
Scroll left and right to view all the KRAs.

## FAB Button

The FAB (Floating Action Button) at the bottom of the Hello Screen gives you quick access to add new information. 

-  Click the Fab button to take a look inside.
-  Click Add Lead to add a new lead. 
-  Click Add Partner to add a new partner. 
-  Notice the Scan Button at the top right corner of the screen – use this to scan and autocapture details from the business card. This feature can be enabled for different modules depending on the requirement. 

If you exit without completing the form, you may save it as a Draft to view and complete later.
 
### Inside the Fab Button

-  Click Schedule Activity to schedule activities, in the future, for yourself (User), Customers or Partners. 
-  Click Log Activity to log pastorcompleted activities for yourself (User), Customers or Partners.

With this, we have completed navigating Vymo from the Hello Screen. Next, we’ll take a look at the Hamburger Menu.

## Hamburger Menu

While you can access almost all of Vymo through the Hello Screen, the Hamburger Menu lists a few important shortcuts.

-  Click Leaderboard to view where you stand against your peers. The Leaderboard can be  configured on request.
-  Click Notifications to view andoror resolve all your notifications in one place.
-  Click Reports to view all available reports. All the reports available on Vymo Web can be automatically pulled up here.
 
### Inside the Hamburger Menu

-  Click All Drafts, to view all your drafts in one place. 
-  Click Settings to view all user and app related details as shown here.

To logout of Vymo, click Logout under Account Settings.
