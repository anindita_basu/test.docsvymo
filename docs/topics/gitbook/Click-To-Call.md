# Click-To-Call

Place online calls using a cloud telephony solution (through a server) to your leads/partners via Vymo Mobile & Web platforms. Vymo detects online calls and auto-logs activities.

The Click-to-call (CTC) feature allows you to use the existing call button in the Leads / Partner details screen to make online calls. Vymo detects online calls based on configuration and auto-logs activities.

## Steps to follow

1.  Login to the Vymo Mobile App and navigate to the Lead or Partner screen.
1.  Click the call button.

You will see a toast message informing you that the request is submitted, and you will receive a call shortly.  

-  You (the logged-in user) will receive a call from the server first. 
-  Answer it, and it will trigger a call to the Lead / Partner. 

After the lead/partner picks up the call, both of you will be connected via the server.

!!! note ""
    If the call does not connect or if the Lead / Partner rejects the call, you will receive a notification and a prompt to try again.
 
## Activity Logging

After the call disconnects, the activity is auto logged in Vymo. 

-  Refresh the screen and to view the activity log (under Completed Activities).
-  Click the activity to view all the details.
-  You can also view the call recording URL in the activity details.
 

## Click-to-call on the Web Platform

Log in to the web platform and navigate to the Leads / Partners section > Opportunities / Partners List page and click the call button. Similar to the Mobile app, the activity is autologged after the call disconnects. 
 

!!! note ""
    Vymo can also integrate with any of your existing cloud telephony solutions to provide the same experience. Please reach out to your Customer Success Manager for more information.

## FAQs

**Who can use the Click-to-call feature on the Command Center?**

Only users for whom Click-to-call is enabled in Vymo can use this feature. It is configurable as per the customer requirements.

**Can the Click-to-call feature be used on all platforms?**

Yes. Click-to-call can be used on the updated Mobile App (both Android & iOS) as well as the Vymo Web platform. 

**The Vymo App is not auto-updating. How to enable this?**

Steps to Enable Auto Update of Apps on Android:

1.	Go to Google Play Store
2.	Click on the three horizontal lines at the top left corner of the screen
3.	Click on Settings
4.	Click on Auto-update Apps
5.	In Auto-update apps: Select either
    1.  Over Wi-Fi only
    1.  Over any network
1.  Click Done

**Can I hear the recorded calls?**

Yes. Call recordings are available only to admin roles by default. Customers can choose whether the call recordings are visible or not.  

**How to retrieve and listen to recorded calls?**

On the web platform:

1.	Login to the Vymo Web Portal
2.	Go to Web Portal > Activities > Activity List
3.	Filter the Activity Type as Outgoing Call
4.	Click Apply Filters
5.	Click on a Record. Scroll down to Profile Section  
7. Click the hyperlink Recordingurl. The audio file will open in a separate tab, click Play to listen
9. To save the call recording file (in mp3 format), right-click on the new tab and select Save As.

On the mobile app, simply click the Activity to open the Activity Details screen and you will see the Recordingurl hyperlink.

**I got this error while placing a call from the Mobile App: "Invalid Call Parameters: Invalid 'URL' or Invalid 'To' specified" What does this mean?**

This means that it is not a valid phone mobile.

**Is Conversation Duration the same as the difference between the call start & end time?**

No. The call start time is marked from the time the agent receives the call (before the customer picks up). However, the conversation duration is calculated from the time when the other person picks up the call. Hence, the conversation duration will always be lesser (around 4-5 sec) than the difference between the call start & call end time.

**Is the Click-To-Call feature the same as the Standard Call Handling?**

No, they’re different. Although both features trigger outgoing calls, in click-to-call, the call is placed through the server and in the call handling feature, the call is placed through the user’s mobile dialer. 

**Can I see the mobile number of the lead/partner?**

You cannot view the number during the call but phone numbers are visible on the lead/partner profile screen by default. Vymo also provides Number Masking if you want to hide Lead/Partner phone numbers on this screen as well from the end-users (who are making the calls).
