# Localization

You can configure your app in a language of your choice. The default language supported is English.

## Languages (Locales) supported

-  Japanese (ja_JP)
-  Vietnamese (vi, vi_VN)
-  Bahasa (id_ID)
-  Thai (th_TH)

## Add a Language

Apart from the supported languages, you can also configure a desired language of their choice. You can provide the translation for all the application related text which will be uploaded in Vymo to show the Web and Mobile application in the required language.

## FAQ

**Do you support multiple languages per client?**

No, a single language is only supported per client. 

**Will the device locale language be honoured in the app?**

After a user logs-in into the app, the configured language is honoured (not the device locale language). 
