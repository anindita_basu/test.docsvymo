# Vymo on Web
  
Sales Managers plan for sales objectives, hire, train and manage Sales teams. In order to do this successfully, they will need tools to track field sales performance, live updates on where their sales agents are, where the leads are in the nurturing life cycle and so on.

The Vymo Web application will enable Sales Managers to get a bird's view of:

-  Dashboard - Sales pipeline, leads in different stages etc. 
-  Activities performed by team members.
-  Track user's daily field sales activities.
-  Analyse Reports on sales and user performance.

**Leads:** You can view/managed Leads in the Vymo Web application. You can add leads or view leads in which sales stage they are.

**Partners:** You can view/manage Partners present in your system. You can view who is assigned to the Partner, Partner coverage and partner engagement. 

**Activities:** View meetings by Sales team member, schedule new meetings/activities for future planning.

**Reports:** View standard reports on Sales and User performance. 

## Login

The login URL for your company will be provided during your setup. Generally the Vymo Web application URL is: `https://<company_code>.lms.getvymo.com/`. 

Ask your company's Administrator for the right details.

## Account Lockout

The user's account can get locked out of the Vymo mobile app if they have entered the wrong password on the password screen multiple times (beyond the permissible number of attempts). To unlock the account, they can contact their manager. The manager can unlock the user from the Manage Users Page. In case, if you need further help, contact  Vymo Support.

1.  Navigate to Vymo Web application > Settings > Manager Users and search for the user.
1.  Click **Locked** and click **Unlck**.
