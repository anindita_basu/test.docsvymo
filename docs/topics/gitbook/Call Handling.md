# Call Handling

Vymo detects calls to your Leads/Partners, automates sales activity update without manual update.

Vymo can handle incoming/outgoing calls with your Leads/Partners. You can also initiate calls from within the Vymo app based on an upcoming meeting reminder or from the Lead/Partner information page.

!!! note ""
    Calls to or from your Leads or Partners are only logged. 

Vymo auto-detects calls to or from your Lead/Partners (incoming, outgoing, missed calls) initiated from the app & ensures that no customer-representative communication is lost. In case a call doesn't go through, Vymo guides the user to set reminders and avoid missed opportunities. 

Every call is a signal for the app to find next best action for the agent. For example: 

-  Incoming/Outgoing calls - You will be triggered to update status after the call
-  Missed calls - Prompts you to set a reminder for call back

## FAQ

**How are missed calls handled?**

For missed calls, Vymo Mobile app displays a notification/pop-up mentioning next possible actions:

-  Send a text
-  Schedule a reminder
-  Call back

**How are repeat calls handled?**

In case of calls from the same number, Vymo records and displays the number of times a particular number was dialled.

**Does call handling work when the app is offline?**

Call handling doesn’t work when the device is offline. But in case a call is made from Vymo, by clicking the dialer option, then Vymo shows call handling pop-up. The number of calls, duration of each call, will be logged in engagement history of the Lead/Partner.

**Complying with Google's Android policy changes**

In January-2019 [Google announced an Android policy change](https://android-developers.googleblog.com/2019/01/reminder-smscall-log-policy-changes.html) that prevents apps from requesting for SMS & Call log permissions. This impacts many apps including Vymo and we have filed multiple permission declaration requests with Google and received an extension, only till March 9th. Due to this, with the latest Vymo app update version 2.3.84 released on 7-March-2019, we are complying with the revised Android policy.

**What is changing?**

Starting Mar 9 2019, Google is revoking SMS & Call Log permissions. All existing applications currently on Play Store must be updated before Mar 9 2019, to comply with this policy.

**What is the impact of this policy change?**

The impact of this policy change is dependent on the Android version in use on the end user's device. Vymo uses device call logs to get the phone number of the last incoming / outgoing / missed call and uses that number to identify the lead or customer the call should be logged against.

On Android 8 or below, there is minimal impact, because an application can still get the phone number of the last call without having to read the device call logs. For very short outgoing calls, the detected duration maybe slightly inaccurate, but Vymo will still be able to log the call against the right lead or customer.

On Android 9 or above, Vymo will not be able to get the number of the last call without the permission to read call logs. Hence Vymo will not be able to detect any incoming calls or outgoing calls placed from the phone dialer. however, outgoing calls placed directly from Vymo app (by clicking on the phone button from a lead or customer's profile) will still be detected and auto logged.

Currently less than 5% of our users have Android 9 installed on their devices, so the immediate impact is only on these users.

**How is Vymo handling this?**

Vymo has submitted an updated version of its Android application that complies with the policy.

**How will Vymo handle this change in the long term?**

The policy change applies only to applications distributed through Play Store. There are other alternatives to distribution, like providing direct links to download the app from Vymo's own website, or distributing through mobile device management (MDM) solutions. 
These other distribution channels are not affected by this policy change. In other words, the Android OS (including versions 9 and above) still allows for apps to read device call logs, which Vymo can use once the user grants them the permission, but it is only applications distributed through Google Play Store that cannot use this permission.

For customers that are already using their own MDM solutions, we will work with them to provide updated versions of Vymo app that retains the permission to read call logs. For customers that don't currently use any MDM solutions, we are exploring ways to provide a hosted MDM solutions. We will have another update shortly to give more information regarding this.

For any questions, concerns or further information, please send an email to support@getvymo.com
