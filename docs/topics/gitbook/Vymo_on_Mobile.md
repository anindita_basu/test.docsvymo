# Vymo on Mobile

Vymo on Mobile is available for your sales agents or reps and managers to view the leads or partners or contacts. Users can also view sales activities and meetings with the prospects, while on the field. 

Your Managers and Sales Agents can use the Vymo Mobile application to do the following tasks:

-  Update sales information on the smartphone or tablet.
-  Stay up to date with sales activities in the team while on the move.
-  Track and measure sales performance.
-  Managers can have a 360 view of their team.

**Home Screen:** You can access critical informationorinformation summary while working on the field.

**Calendar:** Manage activitiesortasks to work on or log details about completed activities.

**Lead List and Lead Details View:** Act upon LeadsorPartners, plan for engagement activity, and reduce the sales cycle time.

**Contacts:** You can call or e-mail leads or contacts and reach out to them.

**Meet Online:** Meet your customers online using Whatsapp or Zoom through Vymo app.

**Notifications:** Receive notifications when new leads are assigned or reminders for upcoming events.

**KRA:** Key Results and Achievements (KRA) at one place.

**Reports:** Access business intelligence reports which include data on activity, sales and revenue trends and winorloss reasons.

## Installation

You can download the Vymo mobile app from the Apple App store (iOS) and Google Play store (Android) platforms.

1.	Contact the Vymo Administrator in your organisation to obtain a user name. 
2.	Once your Vymo account is configured, you will receive a One Time Password (OTP) on your registered mobile phone number, which you can use to login to Vymo.
3.	If the user account is locked out of the Vymo mobile app, learn how to unlock the account.

## Minimum Device Requirements and Permissions

### iOS

-  iOS version 9 and above
-  Minimum memory required: 1GB
-  Rooted devices are not supported
-  Local storage for offline data storage (when data network connectivity is lost)
-  Permissions required for intelligent detection and suggestions: 
-      Call
-      Contacts/Phonebook
-      Location
-      Camera

### Android

-  Android version 4.4 and above
-  Minimum memory required: 1GB
-  Rooted devices are not supported
-  Local storage for offline data storage (when data network connectivity is lost)
-  Permissions required for intelligent detection and suggestions: 
-      Call
-      Contacts/Phonebook
-      Location
-      Camera

## FAQ

**How secure is the communication with the Vymo Mobile app?**

All traffic to and from the Vymo Mobile application and Vymo Servers is secured via HTTPS. The data is compressed and sent, so around 20-30% compression is achieved across a variety of traffic patterns.

**Why does Vymo need to know my location?**

-  Vymo uses location information to generate location based intelligence which drives location-dependent features: 
-  Daily Route, Nearby and suggested meetings. 
-  Lead Allocation can happen more effectively based on the location information. 
-  Reimbursement reportsorexpenses can be shared based on the distance travelled. 

**How accurate is location tracking?** 

The accuracy of location tracking depends on a number of parameters such as user location - indoor or. outdoor, you are traveling or in office, signal strength, data connectivity etc.

**How is the location captured in Vymo?**

When you have logged into Vymo, the mobile app sends its location once every 15 mins in Android to Vymo servers. For iOS, device reports new location co-ordinates whenever the user's actual location changes. Vymo send the location approximately 40 times a day. If you are offline, the location data will not be collected. The location data is stored on Vymo servers for 60 days and deleted automatically.
