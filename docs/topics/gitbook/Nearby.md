# Nearby

Get a quick view of Leads/Partners nearby, plan for ad-hoc meetings when planned activities are rescheduled or declined.

Sales agents can view the Leads/Partners in their current vicinity on the map. These are Leads/Partners assigned to the Sales agents. All meetings of the day and all partners/Leads assigned located in 10 kilometre radius from the current location will be listed. Each Lead/Partner will be represented as a pins on the map.

Vymo will be able to suggest meetings & activities for the user to complete basis free space on their calendar. This feature uses location & route data to identify what’s Nearby.

!!! note ""
    The Nearby radius distance (10km) can be configured. It is currently measured in Kilometers.

Login to Vymo mobile application > Tap on View Map in the Calendar > Nearby tab.

iOS

-  Vertical geofencing is supported while showing leads in Nearby.
-  Grouping and showing lead list is supported.
The pins can have different colors by Lead Priority.

Android

-  Clustering of pins (Leads) is not supported.
-  Grouping and showing Lead count is not supported.

If the number of Leads/partners is high, Vymo will group the pins as shown below (and count of Leads/Partners in vicinity will be displayed).