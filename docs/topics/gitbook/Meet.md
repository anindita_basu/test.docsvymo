# Meet

Connect with your customers on WhatsApp or Zoom via the mobile app. The Meet functionality allows you to connect with your leads/customers on WhatsApp and Zoom. 

## Steps to follow

To use this feature:

1.  Navigate to the Leads/Partner Details screen. 
1.  Click the Meet button.
You will get a pop-up asking you to select either WhatsApp or Zoom. 
1.  Select the option of your choice to launch the app and start your conversation.

!!! note "Note Points:"
    -  If the user does not have WhatsApp or Zoom installed on their phone, they will be redirected to PlayStore/AppStore to download the app.
    -  The Meet feature can be configured using self-serve.
    -  Vymo provides support for integrating any of the online video calling tools which can be used to reach out to leads & customers for business purposes.