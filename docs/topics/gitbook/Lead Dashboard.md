# Lead Dashboard

The Leads Dashboard shows details about leads, the age of leads, status of leads (how many won), time taken to meet a lead.
   
Login to your Vymo Web Application > Reports > All Reports > Lead Analytics > Pipeline Dashboard.

Following reports are available in this dashboard.

## Count of New Leads Over the Time

This report shows the number of leads created on a given date. Lead created date is considered for the calculation.
 
## Count of Leads Won

The report shows the number of leads won on a given date. Last updated date is when a lead is marked as won. 
 
## Time to First Meeting

This report shows the average time it took to set up the first meeting with a lead that was created on a given date. Created date is the date when the lead was created.
 
## Lead Lost Over Time

The report given the number of leads lost on a given date. Last updated date is the date when the lead is marked as lost. 
 
## Total Meeting Duration

This report shows the the total amount of time spent on a meeting with the lead. Created date is the date when the lead was created.
 
