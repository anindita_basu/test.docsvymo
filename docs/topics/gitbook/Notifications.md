# Notifications

Get reminders about upcoming meetings and events with your Leads/Partners. Rich notifications with actions to call/email your Leads/ Partners and get things done on-the-go.

Vymo notifies Sales agents/reps to act on high-ROI activities to improve sales outcomes, send reminders about upcoming meetings.

List of Notifications supported out of the box, however not limited to:

<table>
<tr>
<th>Who</th>
<th>Notify about</th>
<th>Description (SMS/Push Notifications)</th>
</tr>
<tr>
<td>Sales User</td>
<td>Not logged in</td>
<td>Send SMS to remind Sales agents/reps to login </td>
</tr>
<tr>
<td>Sales User</td>
<td>No meetings scheduled for today/ tomorrow</td>
<td>Notify Sales agent/reps to schedule activities</td>
</tr>
<tr>
<td>Sales Manager</td>
<td>No meetings scheduled for today/tomorrow</td>
<td>Notify Sales Manager that their team member does not have anything scheduled yet</td>
</tr>
<tr>
<td>Sales User</td>
<td>Backlog</td>
<td>Remind to close past-due tasks in the Backlog (if greater than 10 activities in Backlog)</td>
</tr>
<tr>
<td>Sales User</td>
<td>Meeting Reminder</td>
<td>Remind 30 min before the Calendar meeting</td>
</tr>
<tr>
<td>Sales User</td>
<td>Meeting Notes - Update</td>
<td>Ask Sales agent/rep how did the meeting go, recommend to add notes from the meeting </td>
</tr>
<tr>
<td>Sales User</td>
<td>Meeting Update Follow-up</td>
<td>Reminder to add notes for a meeting which has occurred in the past (at regular intervals after the meeting was completed)</td>
</tr>
<tr>
<td>Sales User</td>
<td>Meeting Update Follow-up</td>
<td>Notify when a new is lead/partner is assigned to a Sales agent/rep </td>
</tr>
<tr>
<td>Sales Manager</td>
<td>Lead Won/dropped</td>
<td>Notify status update on a Lead's status: won/dropped to the Sales Manager with further actions to call the Lead/Partner</td>
</tr>
<tr>
<td>Sales User</td>
<td>Create Activity</td>
<td>Notify when a new task/activity is created</td>
</tr>
<tr>
<td>Sales Manager</td>
<td>Activity Completion</td>
<td>Notify the Sales Manager when their team member has completed the activity</td>
</tr>
<tr>
<td>Sales User</td>
<td>Lead not called</td>
<td>Notify the Sales agent/rep that the lead has not been engaged with the lead for more than 24 hours</td>
</tr>
<tr>
<td>Sales Manager</td>
<td>Lead not called</td>
<td>Notify the Sales manager to help the Sales user contact the Lead</td>
</tr>
</table>

You can view the Notifications in the Vymo Mobile application > Left menu > Notifications.
