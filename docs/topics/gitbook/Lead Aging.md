# Lead Aging

The Lead Aging report explains how old are the leads and if your sales team is responding to leads in a timely manner.

You can

-  Get the number of leads who weren't engaged with for the period of time as well as the last state
-  Get the last time a lead was contacted and the current state of the lead -  Help prioritise actions on leads who were missed out.

Login to the Vymo Web application > navigate to Reports > All Reports > Lead Analytics > Lead Aging Report.

<table>
<tr>
<th>Report Column</th>
<th>Description</th>
</tr>
<tr>
<td>Age</td>
<td>Number of days since the lead was last contacted </td>
</tr>
<tr>
<td>Count</td>
<td>Number of leads in each state of the workflow</td>
</tr>
</table>
