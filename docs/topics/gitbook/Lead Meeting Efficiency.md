# Lead Meeting Efficiency

Lead Meeting Efficiency report gives details on how many leads are met for the first time and how many are followed up with two or more meetings.
Login to the Vymo Web application > navigate to Reports > All Reports > Lead Analytics and select a report.

## Reports

Following reports depict how many meetings were done to win the deals, how many required follow-ups etc.

## Total Meetings

This report provides the distribution of total number of leads the sales member met by State Date.

## Meetings Done

This report provides the data wise split of total number fresh, repeat and total number of leads met on the State Date.

<table>
<tr>
<th>Report Legend</th>
<th>Description</th>
</tr>
<tr>
<td>New</td>
<td>Number of leads who were met for the first time</td>
</tr>
<tr>
<td>Repeat</td>
<td>Number of leads who were followed up for a second or more meeting</td>
</tr>
<tr>
<td>Total</td>
<td>Total number of leads met</td>
</tr>
</table>

