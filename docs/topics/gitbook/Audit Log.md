# Audit Log

Audit logs capture events which can show “who” did “what” activity and “how” the system behaved. An administrator or developer can examine all events to understand the feature behaviour. A log file will contain information about what action was attempted and if it was successful.

For troubleshooting and auditing purposes, you can use the Audit capability in Vymo:

## Event Log

List of events being logged:

-  Create Lead/Partner
-  Edit Lead/Partner
-  Delete Lead/Partner
-  Reactivate Lead/Partner
-  Reassign Lead/Partner
-  State change (from New -> Meeting -> Doc Collection etc.)

## Data Log

List of data attributes captured per event:

-  `id`: unique id 
-  `type`: What type of operation (CREATE/EDIT.., etc) 
-  `device`: User device information 
-  `actor`: Who made the action
-  `changes`: Basically stores input fields information during the create/edit action
-  `entity_id`: Lead_code
-  `entity_type`: 
    -  VO: Vymo Objects/ Entities, such as Leads and Partners
    -  TASK (not in current scope)
    -  ACTIVITY (not in current scope)
    -  USER (not in scope)
-  `metadata`: http request id/ source file name in case of bulk upload
-  `status`: 
    -  UNPROCESSED: Before an event occurs, an entry is logged in 'UNPROCESSED' status
    -  SUCCESSFUL: After the event occurs successfully, the log will be updated as 'SUCCESSFUL'
    -  FAILED: If the event fails, the audit log status will be updated as 'FAILED'
