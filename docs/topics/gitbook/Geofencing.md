# Geofencing

Learn how Vymo uses your location to geofence the locations you visit and update meetings upon entry and exit automatically.

You would like to see where your Sales agents/reps are traveling every day and analyse the time spent at each customer location. Additionally, for the Sales agents, updating sales activity about meeting a customer is redundant. 
Geofencing is a location technology to detect a specific customer building/office and detect entry/exit from the location. 
Vymo detects when your users reach the customer's venue and the time spent with the customer at that location. Vymo geofences a radius of ~200m around the customer location. 

!!! note ""
    -  Geofence radius is default to ~200 metres, however this distance can be configured to be higher or lower. 
    -  Circular geofences are only supported.
    -  Geofence will not run when GPS connectivity is unavailable and in Offline mode. 
    -  Android: Running battery saver apps, especially on Android, can impact the performance and accuracy of geofencing, because Vymo will be prevented from running in the background.
    -  iOS: once your users log in, all the calendar activities for that day that have an associated primary address will be geofenced.
    -  If you exit from the Vymo app, it will be re-launched automatically in the background when geofence entry and exit happens. However, when Background App Refresh is disabled, you should open Vymo mobile app to resume the delivery of all location-related events.

Vymo detects the meeting location and the entry & exit from the customer’s location. 

User entry and exit from the fence is used to determine how much time the user has spent at the customer’s location.

Time spent is recorded per lead or partner and can be referred to in the Time spent report.

List of locations to be geofenced is refreshed every hour, when the user's Calendar is updated or when the user's location changes significantly.
 
Vymo requires the user should spend at least 5 minutes at the customer location for a valid geofence detection, otherwise geofencing will not be triggered accurately. If you want to modify this limit, you can initiate a discussion with Vymo Support.

Geofence detection will not work if an Exit event is only fired without a corresponding Entry event. However, geofence detection will work when an Entry event is reported but no Exit is received. In this case Vymo will use the meeting duration to update the time spent at that location.