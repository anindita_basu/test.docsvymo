# Overview

Improve Sales productivity with smart options to get more Sales activities performed without manual intervention.

For your Sales teams, getting your Sales users to focus their time on what’s most important for the business is paramount. Salespeople are making choices all day long on how to spend their time, and helping them make the right choices can add up fast which means more sales and higher velocity. 

To higher productivity is the only way to achieve company success, and enabling your team to be more productive is of top priority. The secret for cultivating sales teams that are highly productive (both in efficiency and effectiveness) requires thinking of nuances which make lives of Sales users more productive, regarding better location detection, meeting reminders and suggestions about the next best action.
