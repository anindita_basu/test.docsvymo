# Global Settings

Configure the default settings for your client/ organization, such as currency, timezone, dashboard settings.

You can configure the behaviour of the Vymo Web application.

-  Business Profile
-  Global Dashboard Settings
-  User Login Reminders
-  Activity Settings
-  Input Field Management
 
## Business Profile

You can provide the organization profile details here.

Note:

-  Global business profile settings will be applied by default to all Modules (Lead/Partner..)
-  However, you can override the settings in each module

<table>
<tr>
<th>Attribute</th>
<th>Description</th>
</tr>
<tr>
<td>Locale</td>
<td>
<ul><li>Select a locale for your app</li>
<li>If multiple languages are supported in your app, then on changing the language, the app will reflect the new language</li></ul>
</td>
</tr>
<tr>
<td>Country</td>
<td>Country of incorporation, where your business is registered</td>
</tr>
<tr>
<td>Timezone</td>
<td>Timezone in which you are operating, all the date and time fields will display information in this timezone</td>
</tr>
<tr>
<td>Currency</td>
<td>
<ul><li>Currency you are operating in, all the price related fields will show this currency</li>
<li>Reports will be displayed in this currency</li></ul>
</td>
</tr>
<tr>
<td>Currency ISO</td>
<td>ISO Code of the currency selected</td>
</tr>
<tr>
<td>Quantity</td>
<td>Select a unit of measurement to be displayed in the application</td>
</tr>
<tr>
<td>Country Calling Code</td>
<td>
<ul><li>Select a country code for accepting phone numbers.</li>
<li>The country code selected here will be added as a prefix for all phone number fields.</li>
<li>While initiating calls from your phone, Vymo will add this as the default country code</li></ul>
</td>
</tr>
</table>

## User Login Reminders

Get your Sales Agents/ reps to login to the Vymo application regularly. 

Login to your Vymo Web application > Settings > Driving Usage Adoption.

Drive daily usage by setting up reminders here:

<table>
<tr>
<th>Settings</th>
<th>Description</th>
</tr>
<tr>
<td>Enable</td>
<td>Enable this setting to begin configuration</td>
</tr>
<tr>
<td>Expiry</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Verify Email?</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Email Verification Skip Count</td>
<td>Number of times your Sales users can skip verifying email address, after the count exceeds, users will be mandated to verify their email address</td>
</tr>
<tr>
<td>Phone Verification Skip Count</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Enable</td>
<td>Choose to send reminders to login </td>
</tr>
<tr>
<td>Template</td>
<td>
<ul><li>Enter a message</li>
<li>You can add merge variables to dynamically add the user's information, such as First name</li></ul>
</td>
</tr>
</table>

## Role configuration

You can create/modify the different user roles used in the Vymo app.

## Saved List

This setting allows you to create standard dropdown list - Saved List - that can be reused in different modules in the app.
