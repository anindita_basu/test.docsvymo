# Global Activity Settings

Enter the default Activity settings to be applied for all Activities, you can override the defaults while creating/modifying an activity.
 
## Enable Activities

You can choose to enable or disable Activities for your Vymo Web application.

## Assign Role

Select the users who can create/update activities. The list of roles will be listed for selection.

## Select Type of Meetings

-  User Activity
-  Engagement Activity
 
## Default Assignees for each Activity 

Select who can be on the meetings:
 
## Edit Activity

You can configure what should be allowed while editing an Activity:
 
## Activity Groups

Activity can be grouped into an Activity group. This will control the customisation on how the activity should be visible to the end user - Mobile app/Web app.
