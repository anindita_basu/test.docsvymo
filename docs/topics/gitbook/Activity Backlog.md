# Activity Backlog

Backlog of past-due items which are yet to be completed will be listed for review and further planning.

Backlog feature in the Vymo Mobile application shows all past-due tasks of the Sales agents/reps and they can refer to that for completing the pending work.

Activity Backlog list is updated once every day (at midnight). The daily activities which are not updated will be added in the Backlog automatically. Managers can understand the Backlog of their team members by referring to the Backlog.

The Backlog is a list of all activities that the user had planned, but could not complete or did not update in the Vymo, this includes:

-  Activities planned by the user
-  Activities assigned to the user
-  Meetings planned with Leads/Partners

!!! note ""
    Leads, Partners, Opportunities or Contacts will not be listed in the Backlog list.

## Clear Backlog

Users can update the Activity or rescheduling the Activity. For instance, if the user had planned a meeting with a Lead 2 days ago, the meeting update would be in the Backlog. To clear this item from the Backlog, the user needs to update the meeting or reschedule the meeting to a future date.

Similarly, for activities that are past due, the user needs to Complete/ Cancel/ Reschedule the activity to remove it from the Backlog.
  
## FAQ

Activities assigned to the user by their manager are included in the Backlog if they are pastdue. However, activities, where the user is invited as a Participant, are not considered in the Backlog - these activities will be accounted for in the Backlog of the owner (assignee) of that Activity.
