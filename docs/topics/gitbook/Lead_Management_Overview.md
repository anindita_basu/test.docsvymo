# Overview

A Lead is a person who shows some interest in your product/service and is a potential customer for your company. They provide some contact information, that can be used by salespeople to try creating communication with.
 
## Create Lead

Sales Agents can create Leads via Vymo Web or Vymo Mobile App.

-  Web: Login to Vymo Web application > Leads > Leads list > click on Add lead.
-  Mobile: Login to your Vymo Mobile app > click on floating (+) button > click on Add Lead.

Updates to lead states regarding follow-up dates are added to the Vymo Calendar automatically

Interactions with the client will be captured by the Sales Agent as part of the lead workflow

Managers can view the real-time updates on leads
  
## Save Draft

You can save your work without entering all the information while creating a Lead. This enables users to pause work with limited information and resume when data is available.  
Learn more about draft functionality.

## View Lead

You can view the lead information by navigating to the lead list and selecting a lead. 

You can view the lead profile information, business generated, completed and upcoming activities with the lead.
 
## Update Lead


## Export Leads

You can download Lead details in bulk from the Lead list view from the Vymo web application. You can perform necessary computations and prepare relevant reports. The default filter is Month To Date (MTD). You can extend the date range upto 60 days.

Login to your Vymo web application > Leads > Leads List > Click on Download CSV. 
 
You can refer the download request by navigating to Vymo Web application > Others menu > Downloads and download the data.
 
The export file will be available upto 1 hour after the download request completes.
If you are not able to view the Others menu in your Vymo web application, contact Vymo Support and we will help you out.
