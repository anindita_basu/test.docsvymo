# Device Security

Enforcing device security requirements

## IMEI lock 

For each user, the administrator can whitelist a specific device (or a set of devices), so that the user can login only from that device. Vymo uses device’s IMEI for whitelisting. Organizations can use this capability to ensure that each device meets all the security requirements before they are allowed to access data, and only those devices that are certified can access the data. 

At each login, user device details including the IMEI is captured and reported in the User Device Details Report. 

!!! note ""
    IMEI capture & lockdown is available only for Android devices. This feature is unavailable on iOS devices.

## User Device Details Report

As part of capturing the IMEI, Device make and model information, the User Device Details Report provides a way for managers and administrators to identify which devices are being used by users in their hierarchy.

The report has the following columns:

-  Vymo user name
-  Operating system
-  Device manufacturer
-  Device model and make
-  Last seen on (date and time)
-  IMEI 1 / Device ID
-  IMEI 2 / Device ID

Clicking on a user’s name in this report drills into a details view showing last 30 days information for the selected user. The details include all the IMEIs for that user in the last 30 days.

## Implementing IMEI lock

By configuring IMEI lock, all Android users will be restriced to using Vymo on devices where the IMEI matches the given IMEI in their User profile. Since users can change their devices, it becomes  challenging for  Vymo Administrators to capture, keep track of the IMEI details of users. Hence, it is recommended to initially deploy Vymo without any IMEI lock configuration. As users log in, their IMEI information is captured in the User Device Details Report.

Data from this report can be used to update the IMEI information of each User in the User Management page. This can be done manually or via bulk upload, contact your Customer Success Manager if you need help.
