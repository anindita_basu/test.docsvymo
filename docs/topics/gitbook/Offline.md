# Offline

Working in remote areas or working without data connectivity, with Vymo's Offline feature, users can continue to work as usual and Vymo will update the data once data connectivity is regained.

You can view, create, and edit information in the Vymo Mobile application when you don't have access to a cellular (mobile) or Wi-Fi network.  

When you lose data network, 

-  User hierarchy (User <> Manager <> Management)
-  The first 50 Leads and/or Partners
-  Data recently created or updated in Vymo in the current session

!!! note ""
    -  Offline feature is available in Android only, it is not supported in the iOS platform.
    -  Reports will not have the latest data updated in Offline mode.
    -  Offline data will be erased when the user logs out.
    -  Users cannot search in Offline mode.
    -  Vymo Mobile iOS version does not have offline support, users must always have network connectivity to work on their Vymo app.

Vymo will indicate that the mobile application has entered Offline mode.

Once data network is restored, data which was created/updated in Offline mode will sync to Vymo now. You can also click to sync information on demand.

## Geo-intelligence in Offline Mode

When the user is offline, Vymo Mobile app tracks the user's location. The geolocations of the user are recorded offline and synchronized once data connectivity is back. This information is used to render the location history as well as other metrics used in Lead Allocation, Suggestions & Nearby features.

## FAQ

**Can I use Vymo Mobile application when there is no network available?**

Yes, you can view, create, and edit records in the application even when you don't have access to a cellular (mobile) or Wi-Fi network. When the network connection is restored, the application will automatically sync the updates you have made on your phone to Vymo.

**How secure is the data when stored offline?**

All data stored offline on your device is encrypted by default. Additionally, data captured while offline will be cleared if the user logs out of the Vymo Mobile application.
 
**Can I create new records offline?**

Yes, you can create, edit, and delete records throughout the application in the offline mode and then sync them once the network connection is restored. You will see a banner telling you that you are in the offline mode when the application detects the network connection is lost.
