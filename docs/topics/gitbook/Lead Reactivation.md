# Lead Reactivation

Leads go through the workflow and can either be won or lost at the end of the workflow. 
Once closed, leads can be reactivated by opening the 

-  Can be done on all “Closed” leads – Won or Lost
-  Reactivation can be done by following one of the steps
   -  Reuse data from an existing Lead into new a Lead 
   -  Reactivate an existing Lead
-  These leads can be assigned to Sales Agents for further follow-up
