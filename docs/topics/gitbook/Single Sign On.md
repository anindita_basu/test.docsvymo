# Single Sign On

Learn how to integrate Identity providers with Vymo to setup SSO.

You can setup an authentication mechanism in . The default authentication mechanism is to manage user ID & passwords within Vymo system aka Vymo Auth.
  
Single sign-on (SSO) is the easiest way to log into applications. Students, teachers, and/or administrators will no longer have to manage and remember multiple usernames and passwords!

As such, there are 3 other authentication mechanisms supported in Vymo:

1.	Vymo: User identity is managed in Vymo 
2.	REST API: where in an external authentication REST service does the password verification 
3.	OAuth & Active Directory: user identity & credentials are stored in Active Directory

## Login via Vymo

In this scenario the forgot password and reset password experience will be supported by Vymo. This is referred to as the VYMO_LOGIN auth protocol.

## Other Web Service (REST API)

If customers want to use a different web service that provides authentication as a service the WEB auth protocol is used. In this scenario, the user name and password are passed through Vymo forms and authenticated with the client provide web service by the Vymo backend using REST API. Though the users are created in Vymo, password management functionality will not be available in Vymo.

## OAuth & Active Directory

For clients with Active Directory, OAuth 2.0 is supported.

### OAuth 2.0 Authentication flow

Here is the authentication flow using OAUTH2,
1.	User enters Login ID in the Vymo app
2.	User gets redirected to the login page of the authentication provider (client website)
3.	User enters their login ID and password
4.	User gets redirected back to Vymo application and can see the hello screen, etc.

A few important things to note regarding the standard OAuth behavior.

-  OAuth does not support session portability i.e. because the user has signed into Vymo using their AD-OAuth credentials, it does not mean that the user has access to all other resources that share the same AD credentials. For example, John@contoso.com logged into Vymo using AD-OAuth and then navigates to his email client; the email client will need to reauthenticate the John.
-  Logging out of the authentication provider does not logout the user from Vymo. For example, John@contoso.com logged into Vymo using AD-OAuth and then navigates to the AD web page and logs out from that website. The Vymo session will remain valid. John needs to logout from Vymo to terminate the Vymo session. This is similar to how OAuth works with sites that support Facebook or Google login.
-  Similarly logging out from Vymo does not logout the user from other Application which share the same credential source. For ex: John@contoso.com  logged into Vymo using AD-OAuth and then logs out of Vymo. But the user will not be logged out of the application till the application session remains valid. This is similar to a user logging into Twitter using their Facebook credentials; logging out from Twitter does not log out the user from Facebook or other websites where Facebook authentication was used. This behavior is also dependent on the AD server configurations like session expiry, keep me signed in configuration etc. 

## Configuring Active Directory to support OAuth
 
This section describes how the Active Directory is to be configured to enable OAuth 2.0.

### Azure Active Directory
 
If the customer already have Azure Active Directory services the following configurations have to be made to support OAuth into Vymo 
Note: Customers using Office 365 already have Microsoft Azure AD.
 
#### Register a Web API with Microsoft Azure AD

1.	Sign in to the Azure portal.
2.	On the top bar, click on your account and under the Directory list, choose the Active Directory tenant where you wish to register your application.
3.	Click on More Services in the left hand nav, and choose Azure Active Directory.
4.	Click on App registrations and choose Add.
5.	Enter the name for the Vymo application, for example ‘vymo-web’ and select 'Web Application and/or Web API' as the Application Type. For the sign-on URL, enter the base URL for the sample, which is `https://staging.lms.getvymo.com` for UAT and `https://{client}.lms.getvymo.com` for production as provided by the delivery team / CSM. Click on Create to create the application.
. While still in the Azure portal, choose your application, click on Settings and choose Properties.
7. Find the Application ID value and copy it to the clipboard.
. For the App ID URI, enter `https://<your_tenant_name>/TodoListService`, replacing `<your_tenant_name>` with the name of your Azure AD tenant.
9. Configure `https://staging.lms.getvymo.com/sso/authenticate/get` as the redirect url for UAT and `https://{client}.lms.getvymo.com/sso/authenticate/get` as the redirect URL for production.

Values which will be required by Vymo for configuration:

1.	Tenant ID 
2.	Client ID
3.	Authority URL which will be `https://login.microsoftonline.com/` by default
4.	resource
5.	Client_secret

#### Register Native App with Microsoft Azure AD

1.	Navigate to Azure Active Directory in the Azure portal.
2.	In the left navigation, select App registrations. Click New app registration at the top. 
3 In the Create page, enter a Name for your app registration. Select Native in Application type.
4.	In the Redirect URI box, enter endpoint, using the HTTPS scheme. This value will be `https://staging.lms.getvymo.com/sso/authenticate/get` as the redirect url for UAT and `https://{client}.lms.getvymo.com/sso/authenticate/get` as the redirect URL for production.
5.	Click Create.
6. Once the app registration has been added, select it to open it. Find the Application ID and make a note of this value.
7. Click All settings > Required permissions > Add > Select an API.
8. Type the name of the App Service app that you registered earlier to search for it, then select it and click Select.
9. Select Access `<app_name>`. Then click Select. Then click Done.

You have now configured a native client application that can access your App Service app. Values which will be required by Vymo for configuration

1.	Tenant ID 
2.	Client ID
3.	Authority URL which will be `https://login.microsoftonline.com/` by default
4.	Resource
5.	Redirect uri configured for Native APP (By default, urn:ietf:wg:oauth:2.0:oob)

#### References

-  Create an Azure Active Directory application has a detailed procedure of registering application and to get the details requested.
-  Creating the application Client ID and Client Secret from Microsoft Azure new portal describes how to register an application from the old Azure portal
  
### On-premises Active Directory server

Customers that do not use Azure Active Directory services can configure their on-premise Active Directory Server to support OAuth. For Vymo mobile applications to work a redirect web page needs to be provided in the login experience vs. an NTLM Challenge-Response pop-up dialog box. The Vymo mobile app will fail to render these pop-ups, so please ensure that either:
 
1 Web Application Proxy is set up for the ADFS server, OR 
2. Form based authentication is enabled

#### Steps to configure in ADFS

1.	Sign into the AD console
2.	Run the command: `Add-ADFSClient -Name "Vymo" -ClientId "some-uid-or-other" -RedirectUri="{redirectUri}"`. Note: redirectUri will be `https://staging.lms.getvymo.com/sso/authenticate/get` for UAT and `https://{client}.lms.getvymo.com/sso/authenticate/get` for production.
3.	Claim needs to be configured in the ADFS to send back the upn. This will be used by the Vymo application for validating the user.

Values which will be required by Vymo:

1.	Authority URL or the URL which Vymo will redirect to
2.	Tenant: the url that follows the authority URL, this will be ‘adfs’ by default.
3.	Client ID
4.	Resource: Same as the relying party trust identifier configured in ADFS

#### References 

[OAUTH2 Authentication with ADFS 3.0](https://blog.scottlogic.com/2015/03/09/OAUTH2-Authentication-with-ADFS-3.0.html) describes the step-by-step process to configure ADFS.

## FAQ

**What is reason for choosing OAuth vs. SAML?**

-  HTTP POST binding is suggested by SAML for larger assertions. But Mobile apps don’t have access to the HTTP POST body. They only have access to the URL, used to launch the application. This means that apps can’t read the SAML token.
-  The big advantage with OAuth2 flows are that the communication from the Authorization Server back to the Client and Resource Server is done over HTTP Redirects with the token information provided as query parameters. 
-  The single most important use case for SAML is that it enables web browser single sign-on, which is inadequate for SSO with the Vymo mobile app 
-  OAuth on the other hand, does not assume the Client to be a web-browser
-  Native mobile applications will just work out of the box with OAuth 2.0, so workarounds are not required.

**Why Vymo does not support SAML for authentication via AD?**

-  Creating a modern, forward looking SSO mechanism
-  Making it easier to integrate with a wide variety of external authentication providers

SAML is an old standard compared to OAuth. OAuth is an open, standards-based protocol that offers better cross-device support and consistent across other applications.
