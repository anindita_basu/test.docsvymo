# Activity Configuration

1.  Configure Input fields
1.  Configure Activities

<table>
<tr>
<th>Activity Type</th>
<th>Call/ Meeting</th>
<th>&nbsp;</th>
</tr>
<tr>
<td>Actions</td>
<td>Create, Complete, Schedule, Reschedule</td>
<td>Only scheduled activities can be rescheduled</td>
</tr>
<tr>
<td>Activities in Future</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Activities in the Past - Logs</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Assignee</td>
<td>Who can be assigned to an activity</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Participants</td>
<td>Who can participate in the activity</td>
<td>&nbsp;</td>
</tr>
</table>