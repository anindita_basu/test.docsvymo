# Salesforce

Sales agents meet prospects, partners and engage with existing customers on a daily basis, but updating the CRM with activity details, minutes of the meeting and the next steps is tedious.

The integration between Vymo and Salesforce helps in syncing activity information seamlessly across platforms for rich and timely activity information. A sales activity could be a meeting, document collection, partner training, sales review etc.

A typical workflow looks like this:

1.	Leads/Opportunities created in Salesforce will be pulled into Vymo 
2.	Sales Agents assigned to work on each Lead/Opportunity can create Sales Activities in Vymo 
3.	Updates to Sales Activities will be synced back to Salesforce immediately
4.	Sales teams/ Managers can view updates about the work in-progress, in real-time

!!! note ""
    -  The integration is available on [Standard edition](https://help.salesforce.com/articleView?id=users_understanding_license_types.htm&type=5) of Salesforce and above.
    -  You need to manage the number of Vymo and Salesforce user licenses.
    -  Based on data from Salesforce, reports will not be created in Vymo automatically.
 
## Benefits

Once the integration is setup:

-  Leads imported from Salesforce will be allocated to Sales agents as configured in Vymo.
-  Any suggestions, meetings, will be available immediately. 
-  KRA cards will show the metrics. 
-  Call handling will be enabled for Leads & Contacts - Sales agents can connect with the Leads or Partners.
-  Geofencing will be enabled for Leads and Opportunities - Meeting location will be updated.
 -  Users can add or update notes in Vymo and the activity data will be synced from Vymo to Salesforce in real-time. 

## Login Mechanism

-  OAuth via Salesforce can be configured in Vymo
-  Vymo mobile app will have the option to login via Salesforce credentials, this will invoke the Salesforce SSO/OAuth flow. After login, the user will be brought back to Vymo.
-  If user logs out from the Vymo mobile app, the Salesforce session will be logged out.
-  If user logs out from Vymo web app, Salesforce session in any other browser tab or window will not be closed.

## Release Process

1.	Setup integration in Vymo UAT and Salesforce Sandbox environments, test the sync
2.	After the integration is validated, promote changes from Vymo UAT to Vymo Prod
3.	Setup integration between Vymo Prod and Salesforce Prod environment 

!!! note ""
    Note: It is mandatory for the Sandbox configuration to be similar to the configuration in  Production, otherwise, the setup and testing will be required to be performed again.

## Installation

Here’s a step-by-step process about how the integration will be set up:
 

### Step 1: Install Vymo Package in Salesforce

To get started with the configuration, install the Vymo package in your Salesforce sandbox instance. To install the package, follow these steps:

1.	Login to your Salesforce account. 
2.	Once logged in, search for Vymo in Salesforce AppExchange or paste this URL in a different browser tab
3.	Replace `<yourCluster>` with the cluster ID of your Salesforce deployment (in this example, the cluster ID is ‘ap4’).
4.	Select Install for all Users > click Install and proceed with the installation.
 
### Step 2: Enable History Fields

History tracking should be enabled for the Lead and Opportunity modules. This is to track and sync the workflow state transition history, for instance, new > meeting > document collection state etc., in Vymo, for funnel tracking and reporting.

To enable History Tracking for Lead or Opportunity module:

1.Navigate to Setup > Object Manager > Lead/Opportunity > click on Set History Tracking.
2. Select Lead Status from the list of fields and click on Save.
 

### Step 3: Create Integration User

#### A.	Enable “Enhanced Profile User Interface”.

This Interface makes profile creation much more intuitive.

1.Go to Users > User Management Settings 
2. Enable Enhanced Profile User Interface option

#### B.	Create an API-Only profile

Create the API Only profile which will be set as the profile type of the integration user account.

1.  Click on Setup
2.	Navigate to Administration > Users > Profiles. 
3.	Click on New Profile
4.	Salesforce will prompt you to clone an existing profile. Select an existing profile to clone from and give the new profile a Name. 
!!! note ""
    Note: The Integration user should have the permission to edit Manage Profiles and Permission Sets.
5.	Once created, edit the name of the new user profile.
6.	Under Administrative Permissions, select API Enabled.
7.	Make sure that the Accounts object is set to Read and View All permission under the Standard Object Permissions.
!!! note ""
    Note: The View All permission grants access to view records across the Organization irrespective of the role.
8.	(Optional) If you want to provide the API-Only user to have access to the Salesforce account dashboards, check the API-Only User option.

#### C. Create User with 'API Only' profile

Once the API-Only profile is set up, [create a user](https://success.salesforce.com/answers?id=90630000000gn2VAAQ) with this profile.

1. Go to Users > In the API Only profile overview page, click View Users.
2. In the User Profiles list, click New.
3.	Click on New User
Note: Read more about [limits on number of users](https://success.salesforce.com/answers?id=90630000000gn2VAAQ) per Salesforce edition.
4.	In the New User page,  
    -  Set User License to Salesforce 
    -  Set Profile as API-Only 
 
### Step 4: Provision client in Vymo

1.	Create a Vymo client and provision it in the Vymo UAT environment. Contact Vymo Support to provision a client for your organization.
2.	Enable the following features in Vymo via backend process:
    1.	Nearby
    2.	Geocoding
    3.	Geofencing
    4.	Call Handling
    5.	MS Outlook Calendar sync
3.	Initiate import of configurations from Salesforce to Vymo via backend process in Vymo
    1.	Object mapping for Business & Custom Object
    2.	Field mapping 
    3.	PII fields will be encrypted and synced
4.	Enable the client in Vymo UAT environment
5.	Setup SSO (OAuth 2.0) in Vymo to authenticate via Salesforce

### Step 5: Initiate first time data import from Salesforce

Once the configuration are setup, the initial data import is triggered to retrieve sales and user information from Salesforce to Vymo. The data will be created in Vymo in the following steps:

1.	Users will be imported and created in Vymo
2.	Leads, Opportunities, Accounts, Contacts, Events and Tasks will be retrieved
3.	Leads will be created in Vymo
4.	Accounts will be created in Vymo
5.	Contacts, Opportunities will be created in Vymo
6. History of state transitions will be updated in Vymo
7. Events/Tasks will be created as Activities in Vymo

Salesforce integration is now setup in your Vymo UAT environment. After validation, you can promote the configuration to production environment and repeat the configuration in Salesforce Production.

## Daily Sync

After the initial setup, subsequent updates between Vymo and Salesforce will happen on a regular basis:

<table>
<tr>
<th>Information</th>
<th>Systems</th>
<th>Time Interval</th>
</tr>
<tr>
<td>New/Updates to Leads, Opportunities, Contacts, Accounts</td>
<td>Salesforce to Vymo</td>
<td>Every 15 min</td>
</tr>
<tr>
<td>New/Updates to Events/Tasks</td>
<td>Salesforce to Vymo</td>
<td>Every 15 min</td>
</tr>
<tr>
<td>New/Updates to Activities</td>
<td>Vymo to Salesforce</td>
<td>Instant</td>
</tr>
</table>

### Push (Vymo to Salesforce)

-  Users will add, edit or update Business Objects and Activities from Vymo app.
-  If a Lead is attempted to be created in Vymo, it will first be created in Salesforce, on success, it will be created in Vymo, else it will show the error as seen in Salesforce. User should fix the error (for instance, phone number or email address format incorrect). If the user cannot fix the error, they should contact the company admin to resolve the issue. 
-  For business objects, users will be able to see all those fields which are visible to them on SF during creation on SFDC. 
-  Objects will be tagged with their source system - Vymo or Salesforce
-  When data is deleted in Salesforce, records will be deleted in Vymo. This is a soft-delete operation, data will be archived for audit purposes.

### Pull (Salesforce to Vymo)

-  Sales information from Salesforce will be imported into Vymo every 15 min
-  Configuration changes, if any, will not be retrieved, this should be done manually

## Manage Data sync

You can manage the Salesforce integration in Vymo, contact Vymo Support to help you with any of the following actions:

### Start/Stop Sync

You can stop the sync between Vymo and Salesforce if required. 
When enabled, all records pending to be exported to Salesforce will be synced

### View Status of sync

-  View status of all records synced between Vymo and Salesforce
    -  Business objects and Custom Objects
    -  Activities or Events

### View Errors

-  You can view errors occurred, if any, as part of the sync to Salesforce by downloading the error file.
-  If the sync fails for any reason, the failed sync attempts will be logged with the error details
-  Errors can be fixed, configurations can be changed and Vymo can retry syncing failed records
    -  Each error record will have information about retries available or to skip from being synced
-  After fixing the errors, the updated file can be uploaded to retry specific records to be synced again. Contact Vymo Support to help you out.  

## Limitations

1.	Vymo will export data to Salesforce in real-time. As part of the integration, the volume of data sent will depend on the API limits as mentioned in this [Salesforce document](https://developer.salesforce.com/docs/atlas.en-us.salesforce_app_limits_cheatsheet.meta/salesforce_app_limits_cheatsheet/salesforce_app_limits_platform_api.htm).
2.	Following field types will not be supported:
    1.	Rich Multimedia field - a field which accepts rich text and image/file upload (Text with text formatting such as Bold, Italics etc., and image/file upload .pdf, .doc, 
.docx, .jpeg, .png etc.) Only the string input will be imported into Vymo 
    2.	Status field such as ‘Lead Status’ dropdown with status lifecycle will not be directly imported into Vymo (as this is a status field updated automatically and does not require users to update) 
    3.	Search text box such as ‘Address Search’ to search for a location will not be supported in Vymo.

## Project Management Notes

The following changes are not automated as part of the Salesforce <> Vymo integration process.
  
The items mentioned below should go through the Change Management (CR) process with Vymo.

1.	Create new Sales User and sync to Vymo
2.	After the Sales user has been created in Vymo, the Sales user will be able to login and access Vymo Mobile app

The below items will require prior intimation and depending on the extent of impact, the man days required will be provided from Vymo.

1.	Update a user attribute in Vymo 
2.	Change in Organization Hierarchy
3.	Change the Role of a Sales Agent

The following changes should be done in Vymo Sandbox/UAT environment, tested, before moving to Production

1.	Any Salesforce configuration changes (this can impact Vymo)
2.	Incremental Configuration changes in Salesforce
