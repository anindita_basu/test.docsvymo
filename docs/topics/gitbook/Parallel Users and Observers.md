# Parallel Users and Observers

Consider a scenario where a senior Sales manager has an Assistant who has work-onbehalf permissions. This Assistant, can be created in Vymo, as a Parallel user. Parallel users are unique in that they are outside the Sales hierarchy and yet have all the permissions of the user that they are supporting. 

## What is a Parallel User?

When a new user is created with a same hierarchical access of a different & existing user, the new user is called a parallel user

## Why is a parallel user created?

-  The User is created to have the Parallel view of particular user in the system IE for example an admin user, who has the entire hierarchy under him would need a user who can monitor the report and generate users so creating a parallel user with the parallel view would help
-  The user shall have all access of the Primary user whom he/she is parallel to hence enabling user to perform all the activities of the mapped parallel user.
  
## Limitations of a Parallel user

-  The User cannot have anyone reporting under them or the user cannot report to anyone (I.e user follows the reporting Hierarchy of the User who they are parallel to)
-  The user when leaving the parallel role needs to be converted as a normal user before the user can be incorporated into the system to have users reporting under them or reporting to some who is available in Hierarchy.
-  When converting the parallel user to a normal user
    -  If the user ID does not exist in the system we can use the ID of the user provided by the team to have the ID created as a parallel user.
    -  If the user ID exists in the system the current ID would be disabled and the new ID would be created with the suffix of _admin attached to the employee code( Eg: UserID_admin)
  
## Visibility of Vymo records to Parallel users

Consider the following scenario

Parallel users P1, P2 are observing actual user AP1 has one lead PL1 assigned to him, P2 has one lead PL2  assigned to him and A has one lead AL1 assigned to him.

In the Mobile App

-  P1 can see PL1 and AL1
-  P2 can see PL2 and AL1
-  A can see AL1 only

In the Web App

-  P1 can see PL1
-  P2 can see PL2 A can see AL1

Effectively, everyone can see only leads assigned to themselves and not others.
