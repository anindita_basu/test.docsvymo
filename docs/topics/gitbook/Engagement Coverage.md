# Engagement Coverage

Engagement Coverage helps understand how much time is spent on engaging with the leads.

Login to your Vymo Web Application > Reports > All Reports > Partner Analytics > Engagement Coverage Intensity. Reports

Following reports are available:

## Engagement Coverage Intensity

Metrics that make up Engagement Coverage Intensity dashboard are:

<table>
<tr>
<th>Report Column</th>
<th>Description</th>
</tr>
<tr>
<td>User</td>
<td>Name of user</td>
</tr>
<tr>
<td>Engagement</td>
<td>Number of completed activities</td>
</tr>
<tr>
<td>Median Engagement Duration</td>
<td>Median of time taken for each of the engagements</td>
</tr>
<tr>
<td>Meetings</td>
<td>Number of completed activities</td>
</tr>
<tr>
<td>Last updated time</td>
<td>Date when the task was marked completed</td>
</tr>
</table>

## Engagement by day

Engagement by day report shows the number of activities completed by the team on daily basis. Engagements here is the total number of engagements done by the user and the team. Last Update Time Task is the date when the give activity was completed.
 
## Median Meeting Duration (By Day)

Median Meeting Duration (By Day) report shows the time spent by team on each meeting/engagement. Median Meeting Duration is the median time of all the engagements done by the user and the team.
 
## Meeting by day

Meeting by day reports the number of activities completed by the team on daily basis.
 
## Engaged By User

Engaged By User reports a user level detail on the number of engagements by the user and and median of time taken for each engagement.
