---
title: Setting up automatic allocation through self serve
---

# Allocating leads automatically

For automatic allocation to work, you define certain rules that compare a specific attribute of a lead or sales person to another attribute of the same data type. For example, you can compare the postal code of a lead to that of a sales person, but you cannot compare it with the date of joining of the sales person because one data is text (or numeric) and the other is in the date-time format. 

The logical operators that you can use to build the evaluation expression depend on the data type of the attribute you use. For example, text data do not lend themselves to comparisons such as *greater than* or *lesser than* but can be used for operators such as *equal to* or *not equal to*. More about parameters is at [Parameters for automatic allocations](leads_rules_parameters.md).

## Flowchart

This flowchart shows the steps to get the entire automatic allocation process to work.

![automatic alloction flowchart](../../images/autoAllocationFlow.png)

This page contains the instructions to define the allocation rules and release them to production (UAT).

## Prerequisites

-  The Data Importer settings are set to *Send it for auto allocation* (**Customize** > **Module Settings** > **Data Importer** > **Advanced Settings** > **What should happen to the record when no user is mapped to it**).
-  A list of sales people is available in the app. If not, see "Adding users" (x-ref, TBD).

## Steps

1.  Log in to the Vymo web app.
1.  Click **Customize** > **Module Settings**. Click the **Edit** icon for the lead module for which you're creating the rules. 
1.  Click **Allocation Settings**.
1.  Create a rules by selecting a parameter, the condition to evaluate, and the value to evaluate against. <p>Here's a HowTo video:<br/><video width="320" height="240" controls><source src="../../images/rules_define.mp4" type="video/mp4">Your browser does not support the video tag.
</video></p>
1.  Add as many rules and rule sets as you need.  The sequence in which you define the rules and sets is the sequence in which the rules are run. The rule sets are evaluated with an *IF-ELSE* logic.  Within a rule set, the rules are evaluated with an *AND* logic. 
1.  Click **Save**. You see a message that the rules are now placed in a Draft condition.
1.  In the message box, click **Go To Release Management** so that you can move the rules permanently into the customer configuration.
1.  Click **Create a UAT release**. Specify the release details and make sure to provide your own email ID because that's where you receive an OTP to continue with the process. Click **Proceed**.
1.   Enter the OTP you received in the email, and click **Submit**. Wait for a while till you see a confirmation message saying the process is complete.

## Results

To see the changes, log out of the app and log in again. Next time that you upload or add leads, these rules start running and the leads are allocated automatically to the sales team. You can see the allocations at **Leads** > **Opportunities List**. If the rules fail to allocate a lead, it is displayed on the list of leads available for manual allocation.

## Limitations

Automatic allocations can be done only for one lead at a time. If you must evaluate several leads simultaneously for potential matches against several people, you need customized rules (see [Customizing lead allocations](leads_customize_allocations.md)).

## Related pages

-  [Examples of allocation rules](leads_rules_examples.md)

{% if audience == "internal" %}

## Review comment

[Leave a review comment](https://docs.google.com/spreadsheets/d/16CcIl7wpuhzyl3zVOxw_RJL_e-iovOTVALRETDH9Yr4/edit?usp=sharing)

{% endif %}