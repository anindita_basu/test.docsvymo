---
title: Page title
authors: AB
---

# {{ page.meta.title }}

!!! Important "Work-in-progress"
    This page is a template.

## H2 heading

...

!!! note ""
    Template.
	
### Steps

1.  Step 1
1.  Step another.

<table>
<tr>
<th>1</th>
<th>2</th>
</tr>
<tr>
<td>3</td>
<td>4</td>
</tr>
<tr>
<td>5</td>
<td>6</td>
</tr>
</table>

## Related pages

-  item 1
-  item 2