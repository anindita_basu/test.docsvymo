---
title: Lead allocations, overview
---

# Lead allocations

You allocate leads to the people on the sales team according to certain rules or conditions that work best for your business scenario. For example, you might review leads and allocate them, one at a time, to those who you think are best placed to close the deal. Or, you might define a set of conditions to evaluate and, based on the results, get the leads allocated automatically to the sales people.

Lead allocation is only for creating a possible match between a potential customer (lead) and a sales person (user). An allocation does not automatically lead to closing a deal. 

Leads can be allocated in any of the following ways:

- Manual: You review a list of unassigned leads and assign them, one by one, to a person on the sales team. See [Allocating leads manually](leads_manual_allocation.md).

- Automatic: You define a set of rules, which are evaluated to find a match and allocate the lead to. The matches are allocated in a round-robin fashion (that is, in a circular queue order) according to an AND-OR evaluation defined in the rule set. The rules look at one lead at a time, and allocate it to a person out of all available people. If, after the rules are evaluated, any lead remains unallocated, it is shown for manual allocation. See [Allocating leads automatically](leads_automatic_allocation.md).

- Customized: You set up rules that are customized for a specific business scenario. These rules go beyond the simple, round-robin matches of automatic allocation, and evaluate the possible matches against prioritization criteria. The evaluation looks at all of the unallocated leads and all of the available sales people at the same time, and finds the person best placed to own that lead. See [Customizing lead allocations](leads_customize_allocations.md).

By default, all leads are sent into the manual allocation pipeline unless automatic allocation or customized allocation is set as the first choice. If automatic or customized allocations are in place, all leads are sent into that pipeline, and any leads that remain unallocated after the allocation process are sent to the manual allocation pipeline.

{% if audience == "internal" %}

!!! danger "Dynamic allocation"
    Vymo doesn't offer dynamic allocations, where leads can be allocated according to rules that constantly recalculate certain metrics and do machine-learning to suggest optimized matches. A certain degree of optimization can be done through custom metrics, but the process does not use machine learning or artificial intelligence yet.

{% endif %}

{% if audience == "internal" %}

## Comparison matrix

<table>
<tr>
<th>&nbsp;</th>
<th>Manual allocation</th>
<th>Automatic allocation</th>
<th>Customized allocation</th>
</tr>
<tr>
<td><b>Location info</b></td>
<td>Required</td>
<td>Required only if needed in a rule set</td>
<td>Required only if needed in a custom metric</td>
</tr>
<tr>
<td><b>Metrics available</b></td>
<td>
<ul><li>Open leads count</li>
<li>Allocated leads count</li>
<li>All logged in users</li>
<li>All users reporting to the person who created the lead</li></ul>
</td>
<td>
<ul><li>Open leads count</li>
<li>Allocated leads count</li>
<li>All logged in users</li>
<li>All users reporting to the person who created the lead</li>
<li>Allocation time within specific hours (Custom)</li>
<li>Distance between lead and all hierarchical users within a radius (Custom)</li></ul>
</td>
<td>Everything in automatic allocation + any other custom metric</td>
</tr>
<tr>
<td><b>Solutions Delivery effort</b></td>
<td>No</td>
<td>Yes for the custom metrics</td>
<td>Yes</td>
</tr>
</table>

{% endif %}

## Related pages

-  [Examples of allocation rules](leads_rules_examples.md)
-  [Parameters for automatic allocations](leads_rules_parameters.md)

{% if audience == "internal" %}

## Review comment

[Leave a review comment](https://docs.google.com/spreadsheets/d/16CcIl7wpuhzyl3zVOxw_RJL_e-iovOTVALRETDH9Yr4/edit?usp=sharing)

{% endif %}