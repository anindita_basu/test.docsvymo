---
title: Specifying lead priority
author: AB
---

# {{ page.meta.title }}

!!! Important "Work-in-progress"
    This page is under construction.

## Assigning leads

See allocations

## Re-assigning leads

...

## Closing and re-opening leads

...

## Related pages

-  Geofencing (x-ref, TBD)
-  Lead states in [Creating the leads module](leads_module.md)
-  Lead workflows in [Creating the leads module](leads_module.md)

## Old text dump

Lead Reassignment

Leads will be assigned based on the allocation rules setup. In case, team Managers or Supervisors want to reassign the leads among users to manage their team's bandwidth, they can use the Lead Reassignment option. This will override Vymo's default lead allocation.

You can re-assign leads from the Vymo's web and mobile apps.

Lead Reactivation

Leads go through the workflow and can either be won or lost at the end of the workflow. 

Once closed, leads can be reactivated by opening the 
Can be done on all “Closed” leads – Won or Lost

Reactivation can be done by following one of the steps

Reuse data from an existing Lead into new a Lead Reactivate an existing Lead
These leads can be assigned to Sales Agents for further follow-up 

 
 
 

 
